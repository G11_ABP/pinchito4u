<?php

require_once(__DIR__."/../model/Code.php");
require_once(__DIR__."/../model/CodeMapper.php");
require_once(__DIR__."/../model/User.php");
require_once(__DIR__."/../model/UserMapper.php");


class CodesController {
  

  protected $currentUser; 
  
  private $codeMapper;    

  public function __construct() {    
    // get the current user and put it to the view
    if (session_status() == PHP_SESSION_NONE) {      
      session_start();
    }
    
    if(isset($_SESSION["currentuser"])) {
      $this->currentUser = new User($_SESSION["currentuser"]);     

    } 

    $this->codeMapper = new CodeMapper();
  }

  public function insertCode() {
    if(isset($_SESSION["currentuser"])) {
      $code = new Code($_POST["code_ID"], NULL, $_SESSION["currentuser"]);
     
      if($this->codeMapper->isValidCode($code)){
        $this->codeMapper->update($code); 
        
        $codes = $this->codeMapper->showCodesUser($_SESSION['currentuser']); 
        $_SESSION['__flasharray__']['codesJPop'] = $codes;   
        header('Location: indexJPopular.php?view=index2');
      } else {
        header('Location: indexJPopular.php?view=index2');
      }
    } else {
      //TODO you should be start session
    }   
  }


  public function showCodesUser() {
      $codes = $this->codeMapper->showCodesUser($_SESSION['currentuser']); 
      $_SESSION['__flasharray__']['codesJPop'] = $codes;            

      header('Location: indexJPopular.php?view=index2');
  }

  public function showInsertJPopular() {
    $_SESSION["__flasharray__"]["viewJPopular"]="index2";  
  }

}

?>