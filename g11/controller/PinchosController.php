<?php

require_once(__DIR__."/../model/Pincho.php");
require_once(__DIR__."/../model/PinchoMapper.php");

require_once(__DIR__."/../model/User.php");
require_once(__DIR__."/../model/UserMapper.php");

require_once(__DIR__."/../model/Code.php");
require_once(__DIR__."/../model/CodeMapper.php");


class PinchosController {


  protected $currentUser;
      
  private $pinchoMapper;
  private $userMapper;  
    private $codeMapper;    

  public function __construct() {  
    if (session_status() == PHP_SESSION_NONE) {      
      session_start();
    }
    
    if(isset($_SESSION["currentuser"])) {
      $this->currentUser = new User($_SESSION["currentuser"]);     

    } else { 
      header('Location: ../../index.php');
  }

    $this->pinchoMapper = new PinchoMapper();
    $this->userMapper = new UserMapper();  
    $this->codeMapper = new CodeMapper();  
  }


  public function index() {}

  public function showAllPinchos() {

    $pinchos = $this->pinchoMapper->findPinchOrg();
    $pinchosA = $this->pinchoMapper->findPinchosAsigned();
    $jpro = $this->userMapper->findAllJPro();
    $pinchosV = $this->pinchoMapper->findpinchosNV();

    $_SESSION["__flasharray__"]["pinchosOrgV"]=$pinchosV;
    $_SESSION["__flasharray__"]["pinchosOrg"]=$pinchos;
    $_SESSION["__flasharray__"]["pinchosAsigned"]=$pinchosA;
    $_SESSION["__flasharray__"]["jPro"]=$jpro;   
  }
  

  public function showPinchosJPop() {

    $pinchos = $this->pinchoMapper->findByJPop($_SESSION["currentuser"]);

    $numCodes = $this->codeMapper->countCodesUser($_SESSION['currentuser']);
    $_SESSION['__flasharray__']['nCodesJPop'] = 3 - $numCodes;   



    $_SESSION["__flasharray__"]["pinchosJPop"]=$pinchos;
    $_SESSION["__flasharray__"]["votesJPop"] = $this->pinchoMapper->checkToVote($_SESSION["currentuser"]);
    
    $_SESSION["__flasharray__"]["viewJPopular"]="index1";

  }

  public function voteJPop() {
    $pinchos = $this->pinchoMapper->voteJPop($_POST["nombrePincho"], $_POST["emailEst"], $_SESSION["currentuser"]);
    header('Location: indexJPopular.php');
  }

  public function checkVoteJPop() {

    $pinchos = $this->pinchoMapper->checkVoteJPop($_POST["nombrePincho"], $_SESSION["currentuser"]);

  }

  public function validatePincho() {
    if(isset($_POST["nombrePincho"], $_POST["emailEst"])) {

      $pincho = $this->pinchoMapper->validate($_POST["nombrePincho"], $_POST["emailEst"]); 
      header('Location: indexOrganizador.php?view=index3');
    }     
  }


  public function showPinchosJPro() {

    $pinchos = $this->pinchoMapper->findByJPro($_SESSION["currentuser"]);
    $_SESSION["__flasharray__"]["pinchosJPro"]=$pinchos;
    $_SESSION["__flasharray__"]["valuesJPro"] = $this->pinchoMapper->checkToValue($_SESSION["currentuser"]); 

  }


  public function voteJPro() {

    $pinchos = $this->pinchoMapper->voteJPro($_POST["pinchoNombre"], $_SESSION["currentuser"], $_POST["emailEst"], $_POST["valoracion"]);

    header('Location: indexJProfesional.php');

  }

  public function assignPincho() {

    $this->pinchoMapper->assignPincho($_POST["jpro"], $_POST["nombrePincho"], $_POST["emailEst"]);

    header('Location: indexOrganizador.php?view=index4');
  }

  public function opinions($est, $pincho) {
      $opinions=$this->pinchoMapper->opinions($est, $pincho);
      $_SESSION["__flasharray__"]["opinions"]=$opinions;

  }

  public function addOpinion() {
      $opinions=$this->pinchoMapper->addOpinion($_SESSION["currentuser"],$_POST["pincho"], $_POST["est"],$_POST["opinion"]);

      header('Location: opinions.php?est='.$_POST["est"].'&pincho='.$_POST["pincho"].'');


  }





}

?>