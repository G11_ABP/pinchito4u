<?php

require_once(__DIR__."/../model/Establishment.php");
require_once(__DIR__."/../model/EstablishmentMapper.php");

require_once(__DIR__."/../model/Pincho.php");
require_once(__DIR__."/../model/PinchoMapper.php");

require_once(__DIR__."/../model/Code.php");
require_once(__DIR__."/../model/CodeMapper.php");


class EstablishmentsController {


  protected $currentEstablishment;
  private $establishmentMapper;    
  private $pinchoMapper;
  private $codeMapper;    

  public function __construct() {  
    // get the current user and put it to the view
    if (session_status() == PHP_SESSION_NONE) {      
      session_start();
    }
    
    if(isset($_SESSION["currentEstablishment"])) {
      $this->currentEstablishment = new Establishment($_SESSION["currentEstablishment"]);      
    } 
    
    $this->establishmentMapper = new EstablishmentMapper();
    $this->pinchoMapper = new PinchoMapper();
    $this->codeMapper = new CodeMapper();
  }


  public function index() {}

  public function login() {
    if (isset($_POST["login_email"])){ 

      if ($this->establishmentMapper->isValidEstablishment($_POST["login_email"], $_POST["login_passwd"])) {

        $dataEst = $this->establishmentMapper->recover($_POST["login_email"]);

        $_SESSION["currentEstablishment"]=$_POST["login_email"];

        $_SESSION["dataEstablishment"]=$dataEst;        

        header('Location: view/users/indexEstablecimiento.php'); 

      }else{
        // TODO USUARIO NO VALIDO
    header('Location: index.php'); 
      }
    }           
    // ERROR No se puede loguear   
  } 

  public function register() {
    
    $establishment = new Establishment();
    
    if (isset($_POST["establishment_name"])){ 
      $establishment = new Establishment($_POST["establishment_name"], $_POST["establishmentCIF"], $_POST["establishment_location"], $_POST["establishment_email"], $_POST["establishment_passwd"]);
    	
    	// check if establishment exists in the database
    	if (!$this->establishmentMapper->establishmentExists($establishment->getEstablishment_Email())){
    	
    	  // save the Establishment object into the database
    	  $this->establishmentMapper->save($establishment);     
    	} else {
        // TODO establishmentCIF ya existe
    	}
    }
  
    header('Location: index.php');     
  }


  public function participate() {
    if(isset($_POST["nombrePincho"], $_POST["emailEst"])) {
      //TODO comprobar si existe
      $pinchos = $this->pinchoMapper->findByEst($_POST["emailEst"]);

      foreach($pinchos as $p) {
        if($p["NOMBRE"]!=$_POST["nombrePincho"])  {
          $this->establishmentMapper->participatePincho("0", $p["NOMBRE"], $_POST["emailEst"]);     
        } else {
          $this->establishmentMapper->participatePincho("1", $p["NOMBRE"], $_POST["emailEst"]);           
        } 
           
      }
      //header('Location: indexEstablecimiento.php');    
    } else {
      //TODO los datos no se han enviado correctamente
    }
  }



  public function addPincho() {
    $pincho = new Pincho();

    if(isset($_POST["nombre"], $_POST["email_est"])) {
      //TODO comprobar si existe
      $pincho = new Pincho($_POST["email_est"], $_POST["nombre"], $_POST["descripcion"], $_POST["precio"], $_POST["ingredientes"], $_POST["foto"], $_POST["celiacos"]);
      $this->pinchoMapper->save($pincho);

      header('Location: indexEstablecimiento.php');    
    } else {
      //TODO los datos no se han enviado correctamente
    }
  }

  public function delPincho() {

    if(isset($_POST["nombre"], $_POST["email_est"])) {
      //TODO comprobar si existe
      $this->pinchoMapper->delete($_POST["nombre"], $_POST["email_est"]);

      header('Location: indexEstablecimiento.php');       

    } else {
      //TODO los datos no se han enviado correctamente
    }
  }

  public function editPincho() {

    if(isset($_POST["nombreMod"], $_POST["email_est"])) {
      //TODO comprobar si existe
      $pincho = new Pincho($_POST["email_est"], $_POST["nombreMod"], $_POST["descripcion"], $_POST["precio"], $_POST["ingredientes"], $_POST["foto"], $_POST["celiacos"]);
      $this->pinchoMapper->update($pincho, $_POST["nombre"]);

      header('Location: indexEstablecimiento.php');    
    } else {
      //TODO los datos no se han enviado correctamente
    }
  }


  public function showPinchosEst() {
    $pinchos = $this->pinchoMapper->findByEst($_SESSION["currentEstablishment"]);

    $_SESSION["__flasharray__"]["pinchosEst"]=$pinchos;
  }



  public function requestCode() {
    if($this->codeMapper->numCodesByEst($_SESSION["currentEstablishment"]) < 1000000001){
      do{
        $code = str_replace(" ", "", strtoupper($_SESSION["currentEstablishment"]).rand(0,999999999));      
      } while($this->codeMapper->codeExists($code) != 0);
    } else {
      

    }
    $aux = new Code($code, $_SESSION["currentEstablishment"], NULL);
    $_SESSION["__flasharray__"]["codeGenerator"] = $code;
    $nombreP = $this->pinchoMapper->findPinchoValidate($_SESSION["currentEstablishment"]);

    $this->codeMapper->save($nombreP, $aux);
  
    header('Location: indexEstablecimiento.php?view=index3');     

  }


public function editEst() {
  
    if(isset($_SESSION['currentEstablishment'])) {
      
      if($_POST["password"]==$_POST["confpassword"]) {
        $establishment = new Establishment($_POST["nombre_est"], $_POST["cif"], $_POST["localizacion"], $_SESSION["currentEstablishment"], $_POST["password"]);      

        $this->establishmentMapper->update($establishment);

        $_SESSION["dataEstablishment"]["CIF"]=$establishment->getEstablishmentCIF();
        $_SESSION["dataEstablishment"]["NOMBRE"]=$establishment->getEstablishment_name();
        $_SESSION["dataEstablishment"]["LOCALIZACION"]=$establishment->getEstablishment_location();
        $_SESSION["dataEstablishment"]["PASSWORD"]=$establishment->getEstablishment_passwd();

              } else {
        //TODO la contraseña no coincide 
      }
    } else {
      //TODO los datos no se han enviado correctamente
    }
       //header('Location: indexEstablecimiento.php');  
  
  }



  public function removeEst() {
    $this->establishmentMapper->delete($_SESSION["currentEstablishment"]);

    header('Location: ../../index.php'); 
  }



}

?>