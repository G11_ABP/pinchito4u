<?php

require_once(__DIR__."/../model/User.php");
require_once(__DIR__."/../model/UserMapper.php");


class UsersController {

  protected $currentUser;
   
  private $userMapper;    

  public function __construct() {
    if (session_status() == PHP_SESSION_NONE) {      
      session_start();
    }
    
    if(isset($_SESSION["currentuser"])) {
      $this->currentUser = new User($_SESSION["currentuser"]);      
    } 
    
    $this->userMapper = new UserMapper();
  }

  public function index() {}


    public function register() {
      
      if (isset($_POST["user_name"], $_POST["user_lastname"], $_POST["user_email"], $_POST["user_passwd"])) { 
 
        $user = new User($_POST["user_name"], $_POST["user_lastname"], NULL, $_POST["user_email"], $_POST["user_passwd"]);
        
        
        //TODO check User for register.
        
        // check if User exists
        if (!$this->userMapper->usernameExists($user->getEmail())) {
        
          // save the User object into the database
          $this->userMapper->save($user);     
          header('Location: index.php?register=true');  
        } else {
          header('Location: index.php?register=false1');     
        }     
      } else {
        header('Location: index.php?register=false2');
      }

    }


  public function login() {

    if (isset($_POST["login_email"],  $_POST["login_passwd"])) {

      if ($this->userMapper->isValidUser($_POST["login_email"], $_POST["login_passwd"])) {

        $_SESSION["currentuser"] = $_POST["login_email"];
        $_SESSION["currentTypeUser"] = $this->userMapper->userType($_POST["login_email"]);

        //datos del usuario para modificar
        $_SESSION["datacurrent"] = $this->userMapper->recover($_POST["login_email"]);
       

        switch($this->userMapper->userType($_POST["login_email"])) { 
              
          case 'JURADO POPULAR':
                              $_SESSION["__flasharray__"]["viewJPopular"] = "index2";
                              header('Location: view/users/indexJPopular.php'); 
                              break;
          case 'JURADO PROFESIONAL':
                              header('Location: view/users/indexJProfesional.php'); 
                              break;          
          case 'ORGANIZADOR':
                              header('Location: view/users/indexOrganizador.php'); 
                              break;
        
        
        }
      }else{
        // TODO USUARIO NO VALIDO
    header('Location: index.php'); 
      }
    }           
    // ERROR No se puede loguear   
  } 

  public function logout() {
    session_destroy();
    
    header('Location: ../../index.php');
   
  }


/*-----------------------------------------editar USUARIO--------------------------------------*/
  public function editUser() {
    if(isset($_SESSION['currentuser'])) {
      
      if($_POST["pass"]==$_POST["confpassword"]) {
        $user = new User($_POST["username"], $_POST["apellidos"], $_POST["nombre"], $_SESSION['currentuser'], $_POST["pass"]);      

        $this->userMapper->update($user);


        $_SESSION["datacurrent"]["USERNAME"]=$user->getUser_name();
        $_SESSION["datacurrent"]["APELLIDOS"]=$user->getUser_lastname();
        $_SESSION["datacurrent"]["NOMBRE"]=$user->getUsername();
        $_SESSION["datacurrent"]["EMAIL"]=$user->getEmail();
        $_SESSION["datacurrent"]["PASSWORD"]=$user->getPasswd();

              } else {
        //TODO la contraseña no coincide 
      }
    } else {
      //TODO los datos no se han enviado correctamente
    }
       //header('Location: indexEstablecimiento.php');  
  }


    public function removeUser() {
    $this->userMapper->delete($_SESSION['currentuser']);

    header('Location: ../../index.php'); 
  }
/*-----------------------------------------editar jurado profesional--------------------------------------*/


  public function editJPro() {
    if(isset($_SESSION['currentuser'])) {
      
        $user = new User($_POST["nombre"], $_POST["apellidos"], $_POST["username"], $_POST['emailMod'], $_POST["pass"]);      

        $this->userMapper->updateJPro($user, $_POST["email"]);


      $jpro = $this->userMapper->findAllJPro();
      $_SESSION["__flasharray__"]["jPro"]=$jpro;  


    } else {
      //TODO los datos no se han enviado correctamente
    }
       //header('Location: indexEstablecimiento.php');  
  }


    public function removeJPro() {
    $this->userMapper->delete($_POST['email']);

  }


  public function saveJPro() {
    if(isset($_SESSION['currentuser'])) {
      
        $user = new User($_POST["nombre"], $_POST["apellidos"], $_POST["username"], $_POST['email'], $_POST["pass"]);      

        $this->userMapper->saveJP($user);

    } else {
      //TODO los datos no se han enviado correctamente
    }
       //header('Location: indexEstablecimiento.php');  
  }


  public function search(){
      if(isset($_POST["option"],$_POST["search"])){

        $search = $this->userMapper->search($_POST["option"], $_POST["search"]);
        $_SESSION["__flasharray__"]["search"]=$search;  
      }
  }



}

?>