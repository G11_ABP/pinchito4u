<?php

require_once(__DIR__."/../model/Contest.php");
require_once(__DIR__."/../model/ContestMapper.php");

require_once(__DIR__."/../model/User.php");
require_once(__DIR__."/../model/UserMapper.php");



class ContestsController {

  protected $currentUser;
 
  private $contestMapper;    

  public function __construct() {  
    if (session_status() == PHP_SESSION_NONE) {      
      session_start();
    }
    
    if(isset($_SESSION["currentuser"])) {
      $this->currentUser = new User($_SESSION["currentuser"]);      
    } 

    $this->contestMapper = new ContestMapper();
  }


  public function index() {}

  public function save() {
    
    $contest = new Contest();
    
    if (isset($_POST["contestName"])) { 

      $contest = new Contest($_POST["contestName"], $_POST["logo"], $_POST["startData"], $_POST["endData"], $_POST["rules"], $_POST["prizePro"], $_POST["prizePop"]);
      
      if(!$this->contestMapper->contestExists($contest)) {
        $this->contestMapper->save($contest);     
        header('Location: indexOrganizador.php?save=true');  
      } else {
        header('Location: indexOrganizador.php?save=false1');        
      }
      
    } else {
      header('Location: indexOrganizador.php?save=false2');
    }

  }

  public function addFolleto() {
    $this->contestMapper->addFolleto($_FILES['archivo']);  
  }
}

?>