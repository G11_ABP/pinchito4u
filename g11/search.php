<?php

/**
 * Default controller if any controller is passed in the URL
 */
define("DEFAULT_CONTROLLER", "users");

/**
 * Default action if any action is passed in the URL
 */
define("DEFAULT_ACTION", "index");


function run() {
  // invoke action!
  try {
    if (!isset($_GET["controller"])) {
      $_GET["controller"] = DEFAULT_CONTROLLER; 
    }
    
    if (!isset($_REQUEST["action"])) {
      $_GET["action"] = DEFAULT_ACTION;
    }
    
    // Here is where the "magic" occurs.
    // URLs like: index.php?controller=posts&action=add
    // will provoke a call to: new PostsController()->add()
    
    // Instantiate the corresponding controller
    $controller = loadController($_GET["controller"]);

    // Call the corresponding action
    $actionName = $_GET["action"];
    $controller->$actionName(); 
  } catch(Exception $ex) {
    //uniform treatment of exceptions
    die("An exception occured!!!!!".$ex->getMessage());   
  }
}

function loadController($controllerName) {  
  $controllerClassName = getControllerClassName($controllerName);
  
  require_once(__DIR__."/controller/".$controllerClassName.".php");  
  return new $controllerClassName();
}
 

function getControllerClassName($controllerName) {
  return strToUpper(substr($controllerName, 0, 1)).substr($controllerName, 1)."Controller";
}
 

run();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pincho4U</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/grayscale.css" rel="stylesheet">

    <link href="css/toastr.css" rel="stylesheet"/>

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" style="background-color:white;">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation" style="background-color:black;">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

             <ul class="nav navbar-nav">

                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php"><span class="glyphicon glyphicon-home"></span></a>
                    </li>
              
                </ul>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
               <!-- Busqueda -->
            <form action="search.php?controller=users&amp;action=search" method="POST" class="navbar-form navbar-left" role="search">
                <div class="input-group"> 
                    <div class="input-group-btn">
                        <button type="submit" style="float:left; padding: 6.3px 12px;" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button> 
                        <select class="form-control" style="float:left; border-radius:0px; background-color: rgba(255,255,255,.6); border: #42DCA3;" name="option">                                  
                            <option value="ESTABLECIMIENTO">Establecimiento</option>
                        </select>
                    
                    </div> 
                    <input class="form-control" style="background-color: rgba(255,255,255,.4);"  aria-label="Text input with segmented button dropdown" type="text" name="search" > 
                </div>
            </form>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>


                

  <section id="contact" class="container content-section text-center;padding:0px 0px; border-radius:0px;">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Resultados de la búsqueda</div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example" style="color:black;">
                                <thead>
                                    <tr>
                                        <th>Nombre establecimiento</th>
                                        <th>Email_establecimiento</th>
                                        <th>Localizacion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php      $search = $_SESSION["__flasharray__"]["search"];
                                            foreach($search as $s) { 
                                ?>
                                <tr>
                                        <th><?php 
                                            echo $s["NOMBRE"]; 
                                        ?>
                                        </th>
                                        <th>
                                        <?php 
                                            echo $s["EMAIL"]; 
                                        ?>
                                        </th>
                                        <th>
                                        <?php 
                                            echo $s["LOCALIZACION"]; 
                                        ?>
                                        </th>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>







    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>


    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/grayscale.js"></script>
    <script src="js/toastr.js"></script>
    <script src="../../dist/jquery.flip.min.js"></script>
</body>

</html>
