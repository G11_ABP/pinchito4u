<?php
// file: index.php

/**
 * Default controller if any controller is passed in the URL
 */
define("DEFAULT_CONTROLLER", "users");

/**
 * Default action if any action is passed in the URL
 */
define("DEFAULT_ACTION", "register");

/**
 * Main router (single entry-point for all requests)
 * of the MVC implementation.
 * 
 * This router will create an instance of the corresponding
 * controller, based on the "controller" parameter and call
 * the corresponding method, based on the "action" parameter.
 * 
 * The rest of GET or POST parameters should be handled by
 * the controller itself.
 * 
 * Parameters:
 * <ul>
 * <li>controller: The controller name (via HTTP GET)
 * <li>action: The name inside the controller (via HTTP GET)
 * </ul>
 * 
 * @return void
 * 
 * @author lipido <lipido@gmail.com>
 */
function run() {
  // invoke action!
  try {
    if (!isset($_GET["controller"])) {
      $_GET["controller"] = DEFAULT_CONTROLLER; 
    }
    
    if (!isset($_GET["action"])) {
      $_GET["action"] = DEFAULT_ACTION;
    }
    
    // Here is where the "magic" occurs.
    // URLs like: index.php?controller=posts&action=add
    // will provoke a call to: new PostsController()->add()
    
    // Instantiate the corresponding controller
    $controller = loadController($_GET["controller"]);
    
    // Call the corresponding action
    $actionName = $_GET["action"];
    $controller->$actionName(); 
  } catch(Exception $ex) {
    //uniform treatment of exceptions
    die("An exception occured!!!!!".$ex->getMessage());   
  }
}
 
/**
 * Load the required controller file and create the controller instance
 * 
 * @param string $controllerName The controller name found in the URL
 * @return Object A Controller instance
 */
function loadController($controllerName) {  
  $controllerClassName = getControllerClassName($controllerName);
  
  require_once(__DIR__."/controller/".$controllerClassName.".php");  
  return new $controllerClassName();
}
 
/**
 * Obtain the class name for a controller name in the URL
 * 
 * For example $controllerName = "users" will return "UsersController"
 * 
 * @param $controllerName The name of the controller found in the URL
 * @return string The controller class name
 */
function getControllerClassName($controllerName) {
  return strToUpper(substr($controllerName, 0, 1)).substr($controllerName, 1)."Controller";
}
 
//run!
run();
 
?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Grayscale - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/grayscale.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        var visible = 'personas';      
        function muestra_oculta(id){
            if (document.getElementById){ 
                var hidden = document.getElementById(id);
                if(hidden != document.getElementById(visible)){
                    hidden.style.display = 'block';
                    document.getElementById(visible).style.display = 'none';  
                    visible = (visible == 'personas') ? 'empresas' : 'personas';
                }
            }
        }
    </script>
    
    <script>      
        function mostrar(id){
            if (document.getElementById){ 
                var hidden = document.getElementById(id);
                $(hidden).fadeIn(2000);
            }
        }
        function ocultar(id){
            if (document.getElementById){ 
                var visible = document.getElementById(id);
                $(visible).fadeOut(2000);
            }
        }        
    </script>
    <!--
    transform:
        -moz-transform:       Firefox 
        -webkit-transform:    Webkit 
        -o-transform:         Opera 
        -ms-transform:        IE 9 
    -->

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                    <i class="fa fa-play-circle"></i>  <span class="light">Start</span> Bootstrap
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#register">Login/Register</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">Pincho<span style="font-size:120px;color:red;">4</span>u</h1>
                        <p class="intro-text">A free, responsive, one page Bootstrap theme.<br>Created by Start Bootstrap.</p>
                        <a href="#about" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>About Grayscale</h2>
                <p>Grayscale is a free Bootstrap 3 theme created by Start Bootstrap. It can be yours right now, simply download the template on <a href="http://startbootstrap.com/template-overviews/grayscale/">the preview page</a>. The theme is open source, and you can use it for any purpose, personal or commercial.</p>
                <p>This theme features stock photos by <a href="http://gratisography.com/">Gratisography</a> along with a custom Google Maps skin courtesy of <a href="http://snazzymaps.com/">Snazzy Maps</a>.</p>
                <p>Grayscale includes full HTML, CSS, and custom JavaScript files along with LESS files for easy customization.</p>
            </div>
        </div>
    </section>

    <!-- Download Section -->
    <section id="register" class="content-section text-center">
        <div class="front">    
        <div class="download-section">
            <div id="personas" class="container">
                <div class="col-lg-5" >
                  <!--
                <h2>Download Grayscale</h2>
                    <p>You can download Grayscale for free on the preview page at Start Bootstrap.</p>
                    <a href="http://startbootstrap.com/template-overviews/grayscale/" class="btn btn-default btn-lg">Visit Download Page</a>
                -->  
                    <!-- include register -->  
                    <?php 
                        include("view/users/register.php");
                    ?>          
                </div>
                <div id="rP" class="col-lg-6 col-lg-offset-1">
                    * Haz click <a href="#register" onclick="muestra_oculta('empresas')">aquí</a> para registrar tu empresa. 
                    
                    <div id="recoverPass" class="container-fluid" style="display:none;">
                        <form id="frmRestablecer" role="form" action="validaremail.php" method="post" style=" margin-top: 30px;">
                            <div class="panel panel-default" style="background-color: rgba(51, 51, 51,0.4); border-color: #42dca3;">
                                <div class="panel-heading" style="background-color: rgba(66,220,163,0.4); border-color: #42dca3; font-size: 20px;
                                            padding-top: 5px; padding-bottom: 5px;"> 
                                    Restaurar contraseña 
                                    <button type="button" class="close" onclick="ocultar('recoverPass')">x</button>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <input type="email" id="email" class="form-control" name="email" required placeholder="Correo electrónico">
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-default" value="Enviar" onclick="ocultar('recoverPass')" >
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                
                </div>
            </div>
            <div id="empresas" class="container" style="display:none;">
                <div class="col-lg-5" >
                    <!--
                    <h2>Download Grayscale</h2>
                    <p>You can download Grayscale for free on the preview page at Start Bootstrap.</p>
                    <a href="http://startbootstrap.com/template-overviews/grayscale/" class="btn btn-default btn-lg">Visit Download Page</a>
                    -->  
                    <form role="form">
                        <div class="form-group">
                            <input id="empresa" type="text" class="form-control" placeholder="Nombre de la empresa"/>
                        </div>
                        <div class="form-group" style="clear:both;">
                            <input id="email" type="email" class="form-control" placeholder="Correo electrónico"/>
                        </div>
                        <div class="form-group"> 
                            <input id="pwd" type="password" class="form-control" placeholder="Contraseña"/>
                        </div>
                        <div class="checkbox" style="display:inline; float:left;">
                            <label><input type="checkbox"> Recuerdar mis datos</label>
                        </div>
                        <div style="display:inline; float:right; margin-top:10px; margin-bottom:10px;">
                            <a href="#recuperarContraseña.html">¿Has olvidado tu contraseña?</a>
                        </div>
                        <div class="container-fluid" style="clear:both">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </form> 
                </div> 
                <div class="col-lg-6 col-lg-offset-1">
                    * Haz click <a href="#register" onclick="muestra_oculta('personas')">aquí</a> para registrarte como usuario. 
                </div>                   
            </div>
        </div>
        </div>

        <div class="back">  
        <div class="download-section">
            <div id="personas" class="container">
                <div class="col-lg-5" >
                  <!--
                <h2>Download Grayscale</h2>
                    <p>You can download Grayscale for free on the preview page at Start Bootstrap.</p>
                    <a href="http://startbootstrap.com/template-overviews/grayscale/" class="btn btn-default btn-lg">Visit Download Page</a>
                -->  
                    <!-- include register -->  
                    <?php 
                        include("view/users/register.php");
                    ?>          
                </div>
                <div id="rP" class="col-lg-6 col-lg-offset-1">
                    * Haz click <a href="#register" onclick="muestra_oculta('empresas')">aquí</a> para registrar tu empresa. 
                    
                    <div id="recoverPass" class="container-fluid" style="display:none;">
                        <form id="frmRestablecer" role="form" action="validaremail.php" method="post" style=" margin-top: 30px;">
                            <div class="panel panel-default" style="background-color: rgba(51, 51, 51,0.4); border-color: #42dca3;">
                                <div class="panel-heading" style="background-color: rgba(66,220,163,0.4); border-color: #42dca3; font-size: 20px;
                                            padding-top: 5px; padding-bottom: 5px;"> 
                                    Restaurar contraseña 
                                    <button type="button" class="close" onclick="ocultar('recoverPass')">x</button>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <input type="email" id="email" class="form-control" name="email" required placeholder="Correo electrónico">
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-default" value="Enviar" onclick="ocultar('recoverPass')" >
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                
                </div>
            </div>
            <div id="empresas" class="container" style="display:none;">
                <div class="col-lg-5" >
                    <!--
                    <h2>Download Grayscale</h2>
                    <p>You can download Grayscale for free on the preview page at Start Bootstrap.</p>
                    <a href="http://startbootstrap.com/template-overviews/grayscale/" class="btn btn-default btn-lg">Visit Download Page</a>
                    -->  
                    <form role="form">
                        <div class="form-group">
                            <input id="empresa" type="text" class="form-control" placeholder="Nombre de la empresa"/>
                        </div>
                        <div class="form-group" style="clear:both;">
                            <input id="email" type="email" class="form-control" placeholder="Correo electrónico"/>
                        </div>
                        <div class="form-group"> 
                            <input id="pwd" type="password" class="form-control" placeholder="Contraseña"/>
                        </div>
                        <div class="checkbox" style="display:inline; float:left;">
                            <label><input type="checkbox"> Recuerdar mis datos</label>
                        </div>
                        <div style="display:inline; float:right; margin-top:10px; margin-bottom:10px;">
                            <a href="#recuperarContraseña.html">¿Has olvidado tu contraseña?</a>
                        </div>
                        <div class="container-fluid" style="clear:both">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </form> 
                </div> 
                <div class="col-lg-6 col-lg-offset-1">
                    * Haz click <a href="#register" onclick="muestra_oculta('personas')">aquí</a> para registrarte como usuario. 
                </div>                   
            </div>
        </div>
        </div>

    </section>

    <!-- Contact Section -->
    <section id="contact" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Contact Start Bootstrap</h2>
                <p>Feel free to email us to provide some feedback on our templates, give us suggestions for new templates and themes, or to just say hello!</p>
                <p><a href="mailto:feedback@startbootstrap.com">feedback@startbootstrap.com</a>
                </p>
                <ul class="list-inline banner-social-buttons">
                    <li>
                        <a href="https://twitter.com/SBootstrap" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
                    </li>
                    <li>
                        <a href="https://github.com/IronSummitMedia/startbootstrap" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
                    </li>
                    <li>
                        <a href="https://plus.google.com/+Startbootstrap/posts" class="btn btn-default btn-lg"><i class="fa fa-google-plus fa-fw"></i> <span class="network-name">Google+</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <!-- Map Section -->
    <div id="map"></div>

    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>Copyright &copy; Your Website 2014</p>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/grayscale.js"></script>


    <script src="js/jquery.flip.js"></script>
    <script>
      $(document).ready(function() {
	       $('#register').flip();
      });
    </script>

</body>

</html>


<!-- 
http://animateyourhtml5.appspot.com/pres/index.html?lang=en#21
https://perishablepress.com/tools/css/transform/

https://nnattawat.github.io/flip/

http://www.codedrinks.com/restablecer-contrasena-mediante-correo-electronico-con-php-y-mysql/

http://web.tursos.com/como-hacer-un-sistema-de-registro-de-usuarios-en-php-mysql/
-->