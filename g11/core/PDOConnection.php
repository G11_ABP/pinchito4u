<?php

class PDOConnection {
  private static $dbhost = "127.0.0.1";
  private static $dbport = "3306";
  private static $dbname = "xecbfxpi_abp";
  private static $dbuser = "xecbf_abp";
  private static $dbpass = "Vilartheboss2015";
  private static $db_singleton = null;
  
  public static function getInstance() {
    if (self::$db_singleton == null) {
      self::$db_singleton = new PDO(
        "mysql:host=".self::$dbhost.";port=".self::$dbport.";dbname=".self::$dbname.";charset=utf8", // connection string
        self::$dbuser, 
        self::$dbpass, 
        array( // options
         PDO::ATTR_EMULATE_PREPARES => false,
         PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        )
      );
    }
    return self::$db_singleton;
  }
}
?>