<?php

/**
 * Default controller if any controller is passed in the URL
 */
define("DEFAULT_CONTROLLER", "users");

/**
 * Default action if any action is passed in the URL
 */
define("DEFAULT_ACTION", "index");


function run() {
  // invoke action!
  try {
    if (!isset($_GET["controller"])) {
      $_GET["controller"] = DEFAULT_CONTROLLER; 
    }
    
    if (!isset($_REQUEST["action"])) {
      $_GET["action"] = DEFAULT_ACTION;
    }
    
    // Here is where the "magic" occurs.
    // URLs like: index.php?controller=posts&action=add
    // will provoke a call to: new PostsController()->add()
    
    // Instantiate the corresponding controller
    $controller = loadController($_GET["controller"]);

    // Call the corresponding action
    $actionName = $_GET["action"];
    $controller->$actionName(); 
  } catch(Exception $ex) {
    //uniform treatment of exceptions
    die("An exception occured!!!!!".$ex->getMessage());   
  }
}

function loadController($controllerName) {  
  $controllerClassName = getControllerClassName($controllerName);
  
  require_once(__DIR__."/controller/".$controllerClassName.".php");  
  return new $controllerClassName();
}
 

function getControllerClassName($controllerName) {
  return strToUpper(substr($controllerName, 0, 1)).substr($controllerName, 1)."Controller";
}
 

run();



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pincho4U</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/grayscale.css" rel="stylesheet">

    <link href="css/toastr.css" rel="stylesheet"/>

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">



    <script>
        var visible = 'personas';      
        function muestra_oculta(id){
            if (document.getElementById){ 
                var hidden = document.getElementById(id);
                if(hidden != document.getElementById(visible)){
                    hidden.style.display = 'block';
                    document.getElementById(visible).style.display = 'none';  
                    visible = (visible == 'personas') ? 'empresas' : 'personas';
                }
            }
        }
    </script>
    
    <script>      
        function mostrar(id){
            if (document.getElementById){ 
                var hidden = document.getElementById(id);
                $(hidden).fadeIn(2000);
            }
        }
        function ocultar(id){
            if (document.getElementById){ 
                var visible = document.getElementById(id);
                $(visible).fadeOut(2000);
            }
        }        
    </script>

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <!-- Busqueda -->
            <form action="search.php?controller=users&amp;action=search" method="POST" class="navbar-form navbar-left" role="search">
                <div class="input-group"> 
                    <div class="input-group-btn">
                        <button type="submit" style="float:left; padding: 6.3px 12px;" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button> 
                        <select class="form-control" style="float:left; border-radius:0px; background-color: rgba(255,255,255,.6); border: #42DCA3;" name="option">                                  
                            <option value="ESTABLECIMIENTO">Establecimiento</option>
                        </select>
                    
                    </div> 
                    <input class="form-control" style="background-color: rgba(255,255,255,.4);"  aria-label="Text input with segmented button dropdown" type="text" name="search" > 
                </div>
            </form>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">

                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">Sobre nosotros</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#register">Login/Registro</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contacto</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>





    <!-- http://getbootstrap.com/javascript/ -->
    <div id="reg" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content" style="background-color: rgb(#e6e6e6);border-color: rgb(66, 220, 163);">
          <div class="modal-body">                
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="padding-bottom: 20px;">Ã—</button>


                    <!-- include register -->  
                    <?php 
                        include("view/users/login.php");
                    ?> 
                    <div id="recoverPass" class="container-fluid" style="display:none;">
                        <form id="frmRestablecer" role="form" action="validaremail.php" method="post" style=" margin-top: 30px;">
                            <div class="panel panel-default" style="background-color: rgba(51, 51, 51,0.4); border-color: #42dca3;">
                                <div class="panel-heading" style="background-color: rgba(66,220,163,0.4); border-color: #42dca3; font-size: 20px;
                                            padding-top: 5px; padding-bottom: 5px;"> 
                                    Restaurar contraseÃ±a 
                                    <button type="button" class="close" onclick="ocultar('recoverPass')">x</button>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <input type="email" id="email" class="form-control" name="email" required placeholder="Correo electrÃ³nico">
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-default" value="Enviar" onclick="ocultar('recoverPass')" >
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>


            <div class="clearfix"></div>
          </div>
          <div class="modal-footer" style="background-color: rgb(19, 19, 19);">
	    <div class="checkbox" style="display:inline; float:left;">
        	<label><input type="checkbox"> Recordar mis datos</label>
    	    </div>
          </div>
        </div>
      </div>
    </div>  







    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">Pincho<span style="font-size:120px;color:red;">4</span>u</h1>
                        <p class="intro-text">Descubre la gastronom&iacutea de tu ciudad<br>Concursos de Pinchos de Ourense</p>
                        <a href="#about" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Sobre Pincho4U</h2>
                <p> Una práctica cada vez más habitual, vinculada a la promoción turística en una ciudad, son
                    los “concursos de pinchos”, de “tapas”, o de “platos”. El avance en las nuevas tecnologías,
                    la revolución de las redes sociales, y la expansión de los teléfonos móviles inteligentes
                    están marcando la organización de estos eventos gastronómicos. La publicación de la
                    información en la Web, las redes sociales como espacio abierto de promoción y opinión, la
                    geolocalización y el acceso a infinidad de servicios añadidos que proporcionan los nuevos
                    teléfonos, han abierto un nuevo abanico de posibilidades para el desarrollo de sistemas de
                    información, que ayuden en la organización de estos eventos, y mejoren su visibilidad.
                    Pincho4U es una p&aacutegina donde puedes crear concursos de pinchos para gestionarlos de manera sencilla. 
                    Puedes empezar a formar parte de ella registrandote <a class="page-scroll" href="#register">aqu&iacute</a>.</p>
            </div>
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Folleto Concurso </h2>
                <?php if(file_exists("view/users/folletos/folleto.pdf")) { ?>
                    <p>Haz click <a href="view/users/folletos/folleto.pdf" >aqui</a> para ver el folleto del concurso.</p>
                <?php } else {?>
                    <p>El folleto del concurso no está disponible.</p>
                <?php } ?> 
            </div>
        </div>
    </section>



    <!-- Download Section -->
    <section class="content-section text-center">
        <div id="register" class="download-section">
            <div id="personas" class="container">
                <div style="margin-bottom: 40px;">
                    <h1 style="text-align:center; display:inline;">Usuario</h1>
                    <span>* Haz click <a href="#register" onclick="muestra_oculta('empresas')">aqui</a> si eres una empresa.</span>
                </div>
                <div class="col-lg-5" >


                    <!-- include register -->  
                    <?php 
                        include("view/users/register.php");
                    ?>          
                </div>
                <div id="rP" class="col-lg-6 col-lg-offset-1">
                    
                    
                    
                                        <!-- include login -->  
                    <?php 
                        include("view/users/login.php");
                    ?> 

                
                </div>
            </div>
            <div id="empresas" class="container" style="display:none;">
                <div style="margin-bottom: 40px;">
                    <h1 style="text-align:center; display:inline;">Empresa</h1>
                    <span>* Haz click <a href="#register" onclick="muestra_oculta('personas')">aqui</a> si eres un usuario.</span>
                </div>
                <div class="col-lg-5" >

 
                    <?php 
                        include("view/establishments/register.php");
                    ?>
                </div> 
                <div class="col-lg-6 col-lg-offset-1">
                                        <!-- include login -->  
                    <?php 
                        include("view/establishments/login.php");
                    ?> 

                </div>                   
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Contacta con nosotros</h2>
                <p>Puedes enviarnos las dudas que tengas acerca de la p&aacutegina o sobre el funcionamiento de ella</p>
                <p><a href="mailto:pincho4u.l2jh.com@gmail.com">pincho4u.l2jh.com@gmail.com</a>
                </p>
                <ul class="list-inline banner-social-buttons">
                    <li>
                        <a href="https://twitter.com/UPincho4" target="_blank" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
                    </li>
                    <li>
                        <a href="https://bitbucket.org/G11_ABP/pinchito4u" target="_blank" class="btn btn-default btn-lg"><i class="fa fa-bitbucket fa-fw"></i> <span class="network-name">BitBucket</span></a>
                    </li>
                    <li>
                        <a href="#" target="_blank" class="btn btn-default btn-lg"><i class="fa fa-google-plus fa-fw"></i> <span class="network-name">Google+</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <!-- Map Section -->
    <div id="map"></div>

    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>Copyright &copy; ABP 2015</p>
            <br>
            <p>pinchito4u@pincho4u.l2jh.com</p>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>


    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/grayscale.js"></script>
    <script src="js/toastr.js"></script>
    <script src="../../dist/jquery.flip.min.js"></script>
    <script>
      $(document).ready(function() {
        $('.card a').swipebox();
	$('.card').flip();
      });
    </script>




<?php if(isset($_GET["register"])) {
        if($_GET["register"]=="true"){ ?>
            <script language="javascript" charset="UTF-8">      
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-full-width",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };
                toastr.success("Se ha registrado correctamente. Empiece a disfrutar de PINCHO4U", "Registro correcto");
            </script>        
<?php   } 

        if($_GET["register"]=="false1"){
?>
            <script language="javascript">        
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-full-width",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };
                toastr.error("El usuario introducido ya se encuentra registrado.", "Registro erróneo"); 
            </script>
<?php
        }

       if($_GET["register"]=="false2"){
?>
            <script language="javascript">        
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-full-width",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };
                toastr.error("No se ha registrado correctamente. Vuelva a intentarlo.", "Registro erróneo"); 
            </script>
<?php
        }


    }
?>
</body>

</html>
