<?php

  require_once(__DIR__."/../core/PDOConnection.php");

  class CodeMapper {


    private $db;

    public function __construct() {
      $this->db=PDOConnection::getInstance();
    }

     
    public function save($nombre, $code) {

      $stmt=$this->db->prepare("INSERT INTO `CODIGO`(`CODIGO`, `NOMBRE_PINCHO`, `EMAIL_EST`) VALUES ( ?, ?, ?)");
      $stmt->execute(array($code->getCodeID(), $nombre, $code->getEstablishmentCIF()));  
    }


    public function update($code) {
      //TODO comprobar si ya está existe asginado.
      $stmt=$this->db->prepare("UPDATE `CODIGO` SET `EMAIL_USER`=? WHERE `CODIGO`=?");
      $stmt->execute(array($code->getUser_username(), $code->getCodeID()));  
    }

   public function codeExists($code) {
      $stmt = $this->db->prepare("SELECT count(CODIGO) FROM CODIGO WHERE CODIGO=?");
      $stmt->execute(array($code));

      if ($stmt->fetchColumn() > 0) {
       return 1;        
      } else {
        return 0;
      }
    }

    public function isValidCode($code) {
      $stmt = $this->db->prepare("SELECT * FROM `CODIGO` WHERE  CODIGO = ? && EMAIL_USER IS NULL");
      $stmt->execute(array($code->getCodeID())); 

      if($stmt->rowCount()>0){
        return true;
      } else {
        return false;
      }
    }


    public function numCodesByEst($est){
      $stmt = $this->db->prepare("SELECT EMAIL_EST FROM CODIGO WHERE EMAIL_EST=?");
      $stmt->execute(array($est));

      $rows = $stmt->fetchAll();
       return count($rows);        

    }


    public function countCodesUser($usr){
      $stmt = $this->db->prepare("SELECT * FROM CODIGO WHERE EMAIL_USER=?");
      $stmt->execute(array($usr)); 
    
      $rows = $stmt->fetchAll();
      
      return count($rows);     
    }

    public function showCodesUser($usr){
      $stmt = $this->db->prepare("SELECT CODIGO.*, ESTABLECIMIENTO.NOMBRE AS NOMBRE_EST FROM CODIGO, ESTABLECIMIENTO WHERE CODIGO.EMAIL_EST = ESTABLECIMIENTO.EMAIL && EMAIL_USER=?");
      $stmt->execute(array($usr)); 
    
      $rows = $stmt->fetchAll();
      
      return $rows;     
    }

    
  }

?>