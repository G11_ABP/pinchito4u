<?php
// file: model/UserMapper.php

  require_once(__DIR__."/../core/PDOConnection.php");


  class EstablishmentMapper {


    private $db;

    public function __construct() {
      $this->db=PDOConnection::getInstance();
    }


    public function save($establishment) {

      $nombre = "Certamen de Pinchos OU15";

      $stmt=$this->db->prepare("INSERT INTO `ESTABLECIMIENTO`(`CIF`, `PASSWORD`, `NOMBRE_CONCURSO`, `NOMBRE`, `LOCALIZACION`, `EMAIL`) VALUES (?, ?, ?, ?, ?, ?)");
      $stmt->execute(array( $establishment->getEstablishmentCIF(),
                            $establishment->getEstablishment_passwd(),
                            $nombre,
                            $establishment->getEstablishment_name(),
                            $establishment->getEstablishment_location(),
                            $establishment->getEstablishment_email()
                          )
                    );  
    }


    public function delete($emailEst) {
      $stmt=$this->db->prepare("DELETE FROM ESTABLECIMIENTO WHERE EMAIL=?");
      $stmt->execute(array($emailEst));
    }



    public function update($est) {   
      $stmt = $this->db->prepare("UPDATE `ESTABLECIMIENTO` SET `CIF`=?, `NOMBRE`=?, `LOCALIZACION`=?, `PASSWORD`=? WHERE EMAIL=?");    
      $stmt->execute(array($est->getEstablishmentCIF(), $est->getEstablishment_name(), $est->getEstablishment_location(), $est->getEstablishment_passwd(), $est->getEstablishment_email()));
    }



    public function recover($emailEst) {
      $stmt=$this->db->prepare("SELECT CIF, NOMBRE, LOCALIZACION, EMAIL FROM `ESTABLECIMIENTO` WHERE EMAIL = ?");
      $stmt->execute(array($emailEst));

       return $stmt->fetch(PDO::FETCH_ASSOC);

    }

    public function establishmentExists($establishment) {
      $stmt=$this->db->prepare("SELECT count(EMAIL) FROM ESTABLECIMIENTO WHERE EMAIL=?");
      $stmt->execute(array($establishment));

      if ($stmt->fetchColumn()>0) {   
        return true;
      } 
    }

    public function isValidEstablishment($establishment_email, $passwd) {
      $stmt = $this->db->prepare("SELECT count(EMAIL) FROM ESTABLECIMIENTO WHERE EMAIL=? AND PASSWORD=?");
      $stmt->execute(array($establishment_email, $passwd));

      if ($stmt->fetchColumn() > 0) {
       return true;        
      }
    }


    public function participatePincho($participa, $nombre, $email_est) {
      $stmt = $this->db->prepare("UPDATE `PINCHO` SET `PARTICIPA`=? WHERE NOMBRE=? && EMAIL_EST=?");    
      $stmt->execute(array($participa, $nombre, $email_est));
    }

  }

?>