<?php


  require_once(__DIR__."/../core/PDOConnection.php");

  class UserMapper {


    private $db;

    public function __construct() {
      $this->db=PDOConnection::getInstance();
    }
  
    public function save($user) {

      $stmt=$this->db->prepare("INSERT INTO `USUARIO`(`USERNAME`, `NOMBRE`, `APELLIDOS`, `PASSWORD`, `TIPO`, `EMAIL`) VALUES (?, ?, ?, ?, ?, ?)");
      $stmt->execute(array($user->getUsername(), $user->getUser_name(), $user->getUser_lastname(), $user->getPasswd(),  $user->getType(),  $user->getEmail()));  
    }


    public function usernameExists($email) {
      $stmt=$this->db->prepare("SELECT count(EMAIL) FROM USUARIO WHERE EMAIL=?");
      $stmt->execute(array($email));

      if ($stmt->fetchColumn()>0) {   
        return true;
      } 
    }


    public function isValidUser($username, $passwd) {
      $stmt = $this->db->prepare("SELECT count(EMAIL) FROM USUARIO WHERE EMAIL=? AND PASSWORD=?");
      $stmt->execute(array($username, $passwd));

      if ($stmt->fetchColumn() > 0) {
       return true;        
      }
    }
  

    public function userType($email) {
      $stmt = $this->db->prepare("SELECT TIPO FROM USUARIO WHERE EMAIL=?");
      $stmt->execute(array($email));
      $userType = $stmt->fetch();

      return $userType["TIPO"];
    }


    public function findAllJPro() {
      $stmt = $this->db->query("SELECT * FROM `USUARIO` WHERE TIPO = 'JURADO PROFESIONAL'");    
      $jpro_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $jpro_db;      
    }

   /*-------------------------------modificar datos---------------------------------------------------*/


    public function recover($email) {
      $stmt = $this->db->prepare("SELECT USERNAME, NOMBRE, APELLIDOS, EMAIL, PASSWORD FROM `USUARIO` WHERE EMAIL = ?");    
      $stmt->execute(array($email));
      return $stmt->fetch(PDO::FETCH_ASSOC);   
    }



    public function delete($email) {
      $stmt=$this->db->prepare("DELETE FROM USUARIO WHERE EMAIL=?");
      $stmt->execute(array($email));
    }



    public function update($user) {   
      $stmt = $this->db->prepare("UPDATE `USUARIO` SET `NOMBRE`=?, `APELLIDOS`=?, `PASSWORD`=? WHERE EMAIL=?");    
      $stmt->execute(array($user->getUser_name(), $user->getUser_lastname(), $user->getPasswd(), $user->getEmail()));
    }
    /*-------------------------------modificar datos---------------------------------------------------*/
    public function updateJPro($user,$email) {   
      $stmt = $this->db->prepare("UPDATE `USUARIO` SET  `NOMBRE`=?, `USERNAME`=?, `EMAIL`=?, `APELLIDOS`=?, `PASSWORD`=? WHERE EMAIL=?");    
      $stmt->execute(array($user->getUser_name(), $user->getUsername(), $user->getEmail(), $user->getUser_lastname(), $user->getPasswd(), $email));
    }




    public function saveJP($user) {
      $stmt=$this->db->prepare("INSERT INTO `USUARIO`(`USERNAME`, `NOMBRE`, `APELLIDOS`, `PASSWORD`, `TIPO`, `EMAIL`) VALUES (?, ?, ?, ?, 'JURADO PROFESIONAL', ?)");
      $stmt->execute(array($user->getUsername(), $user->getUser_name(), $user->getUser_lastname(), $user->getPasswd(),  $user->getEmail()));  
    }

    /*-----------------------mostrar busqueda------------------------*/  
    


    public function search($option, $search){
      $stmt = $this->db->query("SELECT * FROM ".$option." WHERE `NOMBRE` LIKE '".$search."%' ");    
      $data_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $data_db;

    }
  }

?>