<?php

class Pincho {

  private $emailEst;
  private $pinchoname;
  private $description;
  private $price; 
  private $ingredients;
  private $foto;
  private $celiacos;
  private $validacion;

  public function __construct($emailEst=NULL, $pinchoname=NULL, $description=NULL, $price=NULL, $ingredients=NULL, $foto=NULL, $celiacos=NULL, $validacion=NULL) {
    $this->emailEst = $emailEst;
    $this->pinchoname = $pinchoname;   
    $this->description = $description;
    $this->price = $price;
    $this->ingredients = $ingredients;
    $this->foto = $foto; 
    $this->celiacos = $celiacos;
    $this->validacion = 0;
  }

  public function getEmailEst() {
    return $this->emailEst;
  }

  public function setEmailEst($emailEst) {
    $this->emailEst = $emailEst;
  }

  public function getPinchoName() {
    return $this->pinchoname;
  }

  public function setPinchoName($pinchoname) {
    $this->pinchoname = $pinchoname;
  }

   public function getDescription() {
    return $this->description;
  }

  public function setDescription($description) {
    $this->description = $description;
  }

  public function getIngredients() {
    return $this->ingredients;
  }  

  public function setIngredients($ingredients) {
    $this->ingredients = $ingredients;
  }

  public function getPrice() {
    return $this->price;
  }

  public function setPrice($price) {
    $this->price = $price;
  }
   
  public function getFoto() {
    return $this->foto;
  }

  public function setFoto($foto) {
    $this->foto = $foto;
  }

  public function getCeliacos() {
    return $this->celiacos;
  }

  public function setCeliacos($celiacos) {
    $this->celiacos = $celiacos;
  }

  public function getValidacion() {
    return $this->validacion;
  }

  public function setValidacion($validacion) {
    $this->validacion = $validacion;
  }

  
}