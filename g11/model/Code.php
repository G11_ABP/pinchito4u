<?php

  class Code {


    private $code_ID;
    private $establishment_CIF;
    private $user_username;


    // Inicializamos las propiedades en null dentro del constructor, si no son enviadas en la llamada del método su valor será null.
    public function __construct($code_ID=NULL, $establishment_CIF=NULL, $user_username=NULL) {
      $this->code_ID=$code_ID;
      $this->establishment_CIF=$establishment_CIF;
      $this->user_username=$user_username;
    }

    public function getCodeID() {
      return $this->code_ID;
    }

    public function setCodeID($code_ID) {
      $this->code_ID=$code_ID;
    }
 
    public function getEstablishmentCIF() {
      return $this->establishment_CIF;
    }
  
    public function setEstablishmentCIF($establishment_CIF) {
      $this->establishment_CIF=$establishment_CIF;
    }
 
    public function getUser_username() {
      return $this->user_username;
    } 
   
    public function setUser_username($user_username) {
      $this->user_username=$user_username;
    }

  }

?>