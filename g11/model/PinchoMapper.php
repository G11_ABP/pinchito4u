<?php

require_once(__DIR__."/../core/PDOConnection.php");

class PinchoMapper {


  private $db;
  
  public function __construct() {
    $this->db = PDOConnection::getInstance();
  }

  public function save($pincho) {   
    $stmt = $this->db->prepare("INSERT INTO `PINCHO` (`EMAIL_EST`, `NOMBRE`, `DESCRIPCION`, `PRECIO`, `INGREDIENTES`, `FOTO`, `CELIACOS`, `VALIDACION`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");    
    $stmt->execute(array($pincho->getEmailEst(), $pincho->getPinchoName(), $pincho->getDescription(), $pincho->getPrice(), $pincho->getIngredients(), $pincho->getFoto(), $pincho->getCeliacos(), $pincho->getValidacion()));

  }


  public function update($pincho, $nombre) {   
    $stmt = $this->db->prepare("UPDATE `PINCHO` SET `EMAIL_EST`=?, `NOMBRE`=?, `DESCRIPCION`=?, `PRECIO`=?, `INGREDIENTES`=?, `FOTO`=?, `CELIACOS`=?, `VALIDACION`=? WHERE NOMBRE=? && EMAIL_EST=?");    
    $stmt->execute(array($pincho->getEmailEst(), $pincho->getPinchoName(), $pincho->getDescription(), $pincho->getPrice(), $pincho->getIngredients(), $pincho->getFoto(), $pincho->getCeliacos(), $pincho->getValidacion(), $nombre, $pincho->getEmailEst()));

  }

  public function delete($nombre, $email_est) {   
    $stmt = $this->db->prepare("DELETE FROM `PINCHO` WHERE NOMBRE=? && EMAIL_EST=?");    
    $stmt->execute(array($nombre, $email_est));
    
  }

  public function findByEst($est) {   
    $stmt = $this->db->prepare("SELECT * FROM PINCHO WHERE EMAIL_EST=?");    
    $stmt->execute(array($est));
    $pinchos_db = $stmt->fetchALL(PDO::FETCH_ASSOC);
   
    return $pinchos_db;
  }

  public function findByJPop($userid) {   
    $stmt = $this->db->prepare("SELECT PINCHO.* FROM `CODIGO`, `PINCHO` WHERE CODIGO.NOMBRE_PINCHO = PINCHO.NOMBRE && CODIGO.EMAIL_EST = PINCHO.EMAIL_EST && CODIGO.EMAIL_USER=?");    
    $stmt->execute(array($userid));
    $pinchos_db = $stmt->fetchALL(PDO::FETCH_ASSOC);
   
    return $pinchos_db;
  }


  public function findByJPro($userid) {
    $stmt = $this->db->prepare("SELECT PINCHO.* FROM ASIGNA_PINCHO, PINCHO WHERE ASIGNA_PINCHO.NOMBRE_PINCHO = PINCHO.NOMBRE && ASIGNA_PINCHO.EMAIL_USER=?");    
    $stmt->execute(array($userid));
    $pinchos_db = $stmt->fetchALL(PDO::FETCH_ASSOC);
   
    return $pinchos_db;    
  }


  public function findPinchoValidate($est) {   
    $stmt = $this->db->prepare("SELECT PINCHO.NOMBRE FROM PINCHO, ESTABLECIMIENTO WHERE PINCHO.EMAIL_EST=ESTABLECIMIENTO.EMAIL && ESTABLECIMIENTO.EMAIL=? && PINCHO.VALIDACION=1");    
    $stmt->execute(array($est));
    $pinchos_db = $stmt->fetch(PDO::FETCH_ASSOC);
  
    return $pinchos_db["NOMBRE"];
  }

  public function findPinchosAsigned() {   
    $stmt = $this->db->query("SELECT * FROM ASIGNA_PINCHO");
    $pinchos_db = $stmt->fetchALL(PDO::FETCH_ASSOC);
  
    return $pinchos_db;
  }

  public function findPinchOrg() {   
    $stmt = $this->db->query("SELECT * FROM PINCHO WHERE VALIDACION='1'");  
    $pinchos_db = $stmt->fetchALL(PDO::FETCH_ASSOC);
  
    return $pinchos_db;
  }

  public function findAll() {   
    $stmt = $this->db->query("SELECT * FROM PINCHO");    
    $pinchos_db = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $pinchos_db;
  }

  public function voteJPop($pinchoid, $estid, $userid) {   
    $stmt = $this->db->prepare("INSERT INTO `VOTA_POPULAR` (`EMAIL_USER`, `NOMBRE_PINCHO`, `EMAIL_EST`, `VOTACION`) VALUES (?, ?, ?, '1');");    
    $stmt->execute(array($userid, $pinchoid, $estid));
  }

  public function voteJPro($pinchoNombre, $emailUser, $emailEst,  $valoracion) {   
    $stmt = $this->db->prepare("INSERT INTO `VALORA_PROFESIONAL`(`EMAIL_USER`, `NOMBRE_PINCHO`, `EMAIL_EST`, `VALORACION`) VALUES (?, ?, ?, ?)");    
    $stmt->execute(array($emailUser, $pinchoNombre, $emailEst, $valoracion));
  }

  public function checkVoteJPop($pinchoid, $userid) {   
    $stmt = $this->db->prepare("SELECT COUNT(*) `VOTA_POPULAR`(`EMAIL_USER`, `NOMBRE_PINCHO`, `VOTACION`) VALUES (?, ?, 1)");    
    $stmt->execute(array($userid, $pinchoid));
  }

  public function checkToVote($userid) {   
    $stmt = $this->db->prepare("SELECT * FROM `VOTA_POPULAR` WHERE EMAIL_USER=?");    
    $stmt->execute(array($userid));
    
    return $stmt->rowCount();
  }

  public function checkToValue($userid) {   
    $stmt = $this->db->prepare("SELECT VALORA_PROFESIONAL.*, ESTABLECIMIENTO.NOMBRE AS NOMBRE_EST FROM `VALORA_PROFESIONAL`,`ESTABLECIMIENTO` WHERE VALORA_PROFESIONAL.EMAIL_EST=ESTABLECIMIENTO.EMAIL && VALORA_PROFESIONAL.EMAIL_USER=?");    
    $stmt->execute(array($userid));
    
    return $stmt->fetchAll();
  }

  public function validate($pinchoid, $userestabl){
    $stmt = $this->db->prepare("UPDATE `PINCHO` SET `VALIDACION` = 1 WHERE NOMBRE=? && EMAIL_EST=?");    
    $stmt->execute(array($pinchoid, $userestabl));
  }

  public function assignPincho($jpro, $nombrePincho, $emailEst) {
    $stmt = $this->db->prepare("INSERT INTO `ASIGNA_PINCHO` (`EMAIL_USER`, `NOMBRE_PINCHO`, `EMAIL_EST`) VALUES (?, ?, ?);");    
    $stmt->execute(array($jpro, $nombrePincho, $emailEst));    
  }

  public function findpinchosNV(){
    $stmt = $this->db->query("SELECT * FROM PINCHO WHERE PARTICIPA='1' && VALIDACION='0'");    
    $pinchos_db = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $pinchos_db;

  }

  public function opinions($est, $pincho){
  $stmt = $this->db->query("SELECT * FROM OPINA_POPULAR WHERE EMAIL_EST='".$est."' && NOMBRE_PINCHO='".$pincho."'");    
  $op = $stmt->fetchAll(PDO::FETCH_ASSOC);

  return $op;
  }

  public function addOpinion($user, $pincho, $est, $opinion) {
    $stmt = $this->db->prepare("INSERT INTO `OPINA_POPULAR` (`EMAIL_USER`, `NOMBRE_PINCHO`, `EMAIL_EST`, `COMENTARIO_POPULAR`) VALUES (?, ?, ?, ?);");    
    $stmt->execute(array($user, $pincho, $est, $opinion));    
  }



}

?>