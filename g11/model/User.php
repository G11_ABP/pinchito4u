<?php

  class User {


    private $user_name;
    private $user_lastname;
    private $username;
    private $user_email;
    private $user_passwd;
    private $user_type;


    // TODO crear función para crear nombre usuario a partir de las dos primeras letras del nombre y del primer apellido. DONE 
    // Inicializamos las propiedades en null dentro del constructor, si no son enviadas en la llamada del método su valor será null.
    public function __construct($user_name=NULL, $user_lastname=NULL, $username=NULL, $user_email=NULL, $user_passwd=NULL) {
      $this->user_name=$user_name;
      $this->user_lastname=$user_lastname;
      if(!isset($username)) {
        $this->username=strtolower($this->strdropaccents(substr($user_name, 0, 2).strtok($user_lastname, " ")));
      } else {
        $this->username=$username;
      }
      $this->user_email=$user_email;
      $this->user_passwd=$user_passwd;    
      $this->user_type="jurado popular";        
    }

  
    public function getUsername() {
      return $this->username;
    }

   
    public function setUsername($username) {
      $this->username=$username;
    }


    public function getUser_name() {
      return $this->user_name;
    }
   
    public function setUser_name($user_name) {
      $this->user_name=$user_name;
    }

 
    public function getUser_lastname() {
      return $this->user_lastname;
    } 
    
    public function setUser_lastname($user_lastname) {
      $this->user_lastname=$user_lastname;
    }

   
    public function getPasswd() {
      return $this->user_passwd;
    }  
 
    public function setPassword($user_passwd) {
      $this->user_passwd=$user_passwd;
    }

 
    public function getEmail() {
      return $this->user_email;
    }  

  
    public function setEmail($user_email) {
      $this->user_email=$user_email;
    }


    public function getType() {
      return $this->user_type;
    }  
 

    public function setType($user_type) {
      $this->user_type=$user_type;
    }



    // TODO validar nombre completo: sin números, sin símbolos y que no estén vacíos. DONE
    // TODO que hacer con nombres compuestos.
    // TODO validar password.
    public function checkIsValidForRegister() {
      $errors=array();
      if (comprobar_nombre($this->user_name)) {
        $errors["user_name"]="El nombre introducido no es válido."; 
      }
      if (comprobar_apellidos($this->user_lastname)) {
        $errors["user_lastname"]="Los apellidos introducidos no son válidos.";
      }      
      if (!filter_var($this->user_email, FILTER_VALIDATE_EMAIL)) {
        $errors["email"]="Introduzca un correo electrónico válido.";              
      }  
      if (strlen($this->passwd)<5) {
        $errors["passwd"]="Password must be at least 5 characters length";  
      }
      if (sizeof($errors)>0){
        throw new ValidationException($errors, "user is not valid");
      }
    } 

    public function strdropaccents($incoming_string){        
      $tofind = "ÀÁÂÄÅàáâäÒÓÔÖòóôöÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ";
      $replac = "AAAAAaaaaOOOOooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn";
      return utf8_encode(strtr( utf8_decode($incoming_string), utf8_decode($tofind), $replac ));
    }

    function comprobar_nombre($nombre_usuario){ 
      if (preg_match("/^[A-Z][a-z]{2,20}$/", $nombre_usuario)) {
        return true; 
      } else { 
        return false; 
      } 
    }

    function comprobar_apellidos($apellidos_usuario){ 
      if (preg_match("/^[[:upper:]][[:lower:]]{2,20}[[:space:]][[:upper:]][[:lower:]]{2,20}$/", $apellidos_usuario)) { 
        return true; 
      } else { 
        return false; 
      } 
    }

  }

?>