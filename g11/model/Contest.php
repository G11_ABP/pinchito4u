<?php
// file: model/User.php

  class Contest {


    private $name;
    private $logo;
    private $startData;
    private $endData;    
    private $rules;
    private $prizePro;
    private $prizePop;
    private $folleto;


    public function __construct($name = NULL, $logo = NULL, $startData = NULL, $endData = NULL, $rules = NULL, $prizePro = NULL, $prizePop = NULL, $folleto = NULL) {
      $this->name = $name;
      $this->logo = $logo;
      $this->startData = $startData;
      $this->endData = $endData;
      $this->rules = $rules;
      $this->prizePro = $prizePro;
      $this->prizePop = $prizePop;
      $this->folleto = $folleto;
    }

 
    public function getName() {
      return $this->name;
    }

  
    public function setName($name) {
      $this->name = $name;
    }

 
    public function getLogo() {
      return $this->logo;
    }

    
    public function setLogo($logo) {
      $this->logo = $logo;
    }
  
    public function getStartData() {
      return $this->startData;
    } 

    
    public function setStartData($startData) {
      $this->startData = $startData;
    }
  
    public function getEndData() {
      return $this->endData;
    } 

    
    public function setEndData($endData) {
      $this->endData = $endData;
    }
    
  
    public function getRules() {
      return $this->rules;
    } 

    
    public function setRules($rules) {
      $this->rules = $rules;
    }

  
    public function getPrizePro() {
      return $this->prizePro;
    } 

    
    public function setPrizePro($prizePro) {
      $this->prizePro = $prizePro;
    }

      
    public function getPrizePop() {
      return $this->prizePop;
    } 

    
    public function setPrizePop($prizePop) {
      $this->prizePop = $prizePop;
    }

 
    public function getFolleto() {
      return $this->folleto;
    } 

   
    public function setFolleto($folleto) {
      $this->folleto = $folleto;
    }


  }

?>