<?php

  class establishment {


    private $establishment_name;
    private $establishmentCIF;
    private $establishment_location;
    private $establishment_email;
    private $establishment_passwd;


    // TODO crear función para crear nombre usuario a partir de las dos primeras letras del nombre y del primer apellido. DONE 
    // Inicializamos las propiedades en null dentro del constructor, si no son enviadas en la llamada del método su valor será null.
    public function __construct($establishment_name=NULL, $establishmentCIF=NULL, $establishment_location=NULL, $establishment_email=NULL, $establishment_passwd=NULL) {
      $this->establishment_name=$establishment_name;
      $this->establishmentCIF=$establishmentCIF;
      $this->establishment_location=$establishment_location;
      $this->establishment_email=$establishment_email;
      $this->establishment_passwd=$establishment_passwd;    

    }

    public function getEstablishment_location() {
      return $this->establishment_location;
    }
  
    public function setEstablishment_location($establishment_location) {
      $this->establishment_location=$establishment_location;
    }

    public function getEstablishment_name() {
      return $this->establishment_name;
    }
   
    public function setEstablishment_name($establishment_name) {
      $this->establishment_name=$establishment_name;
    }
 
    public function getEstablishmentCIF() {
      return $this->establishmentCIF;
    } 
   
    public function setEstablishmentCIF($establishmentCIF) {
      $this->establishmentCIF=$establishmentCIF;
    }

    public function getEstablishment_passwd() {
      return $this->establishment_passwd;
    }  

    public function setEstablishment_password($establishment_passwd) {
      $this->establishment_passwd=$establishment_passwd;
    }

    public function getEstablishment_email() {
      return $this->establishment_email;
    }  
  
    public function setEstablishment_email($establishment_email) {
      $this->establishment_email=$establishment_email;
    }

    // TODO (as USER)
    public function checkIsValidForRegister() {
    } 


  }

?>