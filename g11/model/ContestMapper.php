<?php

  require_once(__DIR__."/../core/PDOConnection.php");


  class ContestMapper {


    private $db;

    public function __construct() {
      $this->db=PDOConnection::getInstance();
    }

     
    public function save($contest) {

      $stmt=$this->db->prepare("INSERT INTO `PINCHITO4U` (`NOMBRE`, `LOGO`, `FECHA_INI`, `FECHA_FIN`, `BASES`, `PREMIO_PRO`, `PREMIO_POP`, `FOLLETO`) VALUES (?, ?, ?, ?, ?, ?, ?, null)");
      $stmt->execute(array($contest->getName(), $contest->getLogo(), $contest->getStartData(), $contest->getEndData(), $contest->getRules(), $contest->getPrizePro(),$contest->getPrizePop()));  
    }

    public function update($code) {

      //TODO comprobar si ya está existe asginado.
      $stmt=$this->db->prepare("UPDATE `CODIGO` SET `EMAIL_USER`=? WHERE `CODIGO`=?");
      $stmt->execute(array($code->getUser_username(), $code->getCodeID()));  
    }

   public function contestExists($contest) {
      $stmt = $this->db->prepare("SELECT count(NOMBRE) FROM PINCHITO4U WHERE NOMBRE=?");
      $stmt->execute(array($contest->getName()));

      if ($stmt->fetchColumn() > 0) {   
        return true;
      } 
    }



    public function updateFolleto($contest,$nombre) {   
      $stmt = $this->db->prepare("UPDATE PINCHITO4U SET FOLLETO=? WHERE NOMBRE=?");    
      $stmt->execute(array($contest,$nombre));
    }

    public function addFolleto($archivo) { 

          $nombre = "folleto.pdf";
          $tipo = $_FILES['archivo']['type'];
          $tamanio = $_FILES['archivo']['size'];
          $ruta = $_FILES['archivo']['tmp_name'];
          $destino = "folletos/".$nombre;
          
          copy($ruta, $destino);
          
      

       return $nombre;
    }

    public function getName() {   
      $stmt = $this->db->query("SELECT * FROM  PINCHITO4U");  
      $nombreC = $stmt->fetch();  
      return $nombreC["NOMBRE"];
    }
    
  }

?>