<?php

define("DEFAULT_CONTROLLER", "pinchos");

define("DEFAULT_ACTION", "showAllPinchos");

function run() {

  try {
    if (!isset($_GET["controller"])) {
      $_GET["controller"] = DEFAULT_CONTROLLER; 
    }
    
    if (!isset($_REQUEST["action"])) {
      $_GET["action"] = DEFAULT_ACTION;
    }
    
    $controller = loadController($_GET["controller"]);
    $actionName = $_GET["action"];
    $controller->$actionName(); 
  } catch(Exception $ex) {
    die("An exception occured!!!!!".$ex->getMessage());   
  }
}
 
function loadController($controllerName) {  
  $controllerClassName = getControllerClassName($controllerName);
  require_once(__DIR__."/../../controller/".$controllerClassName.".php");  
  return new $controllerClassName();
}
 
function getControllerClassName($controllerName) {
  return strToUpper(substr($controllerName, 0, 1)).substr($controllerName, 1)."Controller";
}
 
run();

if($_SESSION["currentTypeUser"] != "ORGANIZADOR") {
 header('Location: ../../not_acceptable.html');
   
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pinchito4u</title>

    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/sb-admin-2.css" rel="stylesheet">
    <link href="../../css/toastr.css" rel="stylesheet"/>    

    <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <script>
            <?php define("DEFAULT_VIEW", "index1"); 
           
            if (!isset($_GET["view"])) {
                $view= DEFAULT_VIEW; 
            } else {
                $view=$_GET["view"];
            }
             ?>

            var visible = <?php echo "'".$view."';"; ?>
        
        function muestra_oculta(id){
            
            if (document.getElementById){ 
                var hidden = document.getElementById(id);
                 
                if(hidden != document.getElementById(visible)){
                    hidden.style.display = 'block';
                    document.getElementById(visible).style.display = 'none';  
                    switch(id){
                        case 'index1':  visible='index1';
                                        break;
                        case 'index2':  visible='index2';
                                        break;
                        case 'index3':  visible='index3';
                                        break;
                        case 'index4':  visible='index4';
                                        break;
                        case 'index5':  visible='index5';
                                        break;
                        case 'index6':  visible='index6';
                                        break;                                         
                    }
                }
            }
        }
    </script>

</head>

<body>

    <div id="wrapper">


        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="indexOrganizador.php">Pinchito4u</a>
            </div>


            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ORGANIZADOR<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                        <form action="indexOrganizador.php?controller=users&amp;action=logout" method="POST">
                        <div class="center"><button class="btn btn-primary center-block">Cerrar Sesión</button></div>
                        </form>
                      </li>
                    </ul>
                  </li>                                   
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#" onclick="muestra_oculta('index1')"> Crear Concurso</a>
                            <a href="#" onclick="muestra_oculta('index2')"> Publicar Folleto</a>
                            <a href="#" onclick="muestra_oculta('index3')"> Validar Pincho</a>
                            <a href="#" onclick="muestra_oculta('index4')"> Asignar Pincho</a>
                            <a href="#" onclick="muestra_oculta('index5')"> Listar/modificar jurados profesionales</a>
                            <a href="#" onclick="muestra_oculta('index6')"> Crear jurado profesional</a>                              
                        </li>                  
                    </ul>
                </div>
            </div>
        </nav>  <!-- END NAVBAR -->

        <div id="page-wrapper">
            <!-- START CREAR CONCURSO -->
            <div id="index1" class="container-fluid"  <?php if($view!="index1"){ echo "style='display:none;'"; }?> >
                <div class="col-md-5">
                    <div class="form-area">  
                        <form action="indexOrganizador.php?controller=contests&amp;action=save" method="POST">  
                            <br style="clear:both">
                            <h3 style="margin-bottom: 25px; text-align: center;">Creación del concurso</h3>
                            <div class="form-group">
                                <input type="text" id="name" class="form-control" placeholder="Nombre del concurso" name="contestName">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Logo" name="logo">
                            </div>                                        
                            <div class="form-group">
                                <input type="date" id="fech_ini" class="form-control" step="1" min="2015-11-01" max="2050-01-01" placeholder="Fecha inicio concurso (YYYY-MM-DD)" name="startData">
                            </div>
                            <div class="form-group">
                                <input type="date" id="fech_fin" class="form-control" step="1" min="2015-11-01" max="2050-01-01" placeholder="Fecha final concurso (YYYY-MM-DD)" name="endData">
                            </div>
                            <div class="form-group">
                                <textarea type="textarea" id="bases" class="form-control" placeholder="Bases" maxlength="500" rows="7" name="rules"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Premio profesional concurso" name="prizePro">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Premio popular concurso" name="prizePop">
                            </div>     
                            <input type="submit" class="btn btn-primary pull-right" value="Crear concurso">
                        </form>
                    </div>
                </div>
            </div>  <!-- END CREAR CONCURSO -->

        <!--START PUBLICAR FOLLETO --> 
            <form action="indexOrganizador.php?controller=contests&amp;action=addFolleto" class="form-horizontal" method="POST" enctype="multipart/form-data">
                <div id="index2" class="container-fluid"  <?php if($view!="index2"){ echo "style='display:none;'"; }?> >
                        <h3 style="margin-bottom: 25px; text-align: center;">Publicar Folleto</h3>
         
                        <div class="col-md-offset-1 col-md-7 form-group">
                           
                            <div class="input-group">

                                <input type="file" class="btn btn-default btn-file"  placeholder="Seleccione un archivo pdf" name="archivo"> 
                           
                        
                                  <span class="input-group-btn">
                                    <input type="submit" class="btn btn-primary" style="border-radius:4px" value="subir">   
                                  </span>
                                </div>  
                        </div>       
                </div>
            </form>
            <!-- END PUBLICAR FOLLETO -->

            <!-- START VALIDAR PINCHO -->
           <div id="index3" class="container-fluid"   <?php if($view!="index3"){ echo "style='display:none;'"; }?> >
               <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Validar Pinchos</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Lista de pinchos para validar
                            </div>
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Email_est</th>
                                                <th>Descripción</th>
                                                <th>Precio</th>
                                                <th>Ingredientes</th>
                                                <th>Foto</th>
                                                <th>Celiaco</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $prueba = $_SESSION["__flasharray__"]["pinchosOrgV"];
                                            $numPinchos = count($prueba);
                                            foreach($prueba as $pincho) {
                                        ?>  
                                            <tr>
                                                <th><?php 
                                                    echo $pincho["NOMBRE"]; 
                                                ?>
                                                </th>
                                                <th>
                                                <?php 
                                                    echo $pincho["EMAIL_EST"]; 
                                                ?>
                                                </th>
                                                <th>
                                                <?php 
                                                    echo $pincho["DESCRIPCION"]; 
                                                ?>
                                                </th>
                                                <th>
                                                <?php 
                                                    echo $pincho["PRECIO"]." €"; 
                                                ?>
                                                </th>                                                           
                                                <th>
                                                <?php 
                                                    echo $pincho["INGREDIENTES"]; 
                                                ?>
                                                </th>
                                                <th>
                                                <?php 
                                                    echo $pincho["FOTO"]; 
                                                ?>
                                                </th>                                                            
                                                <th>
                                                <?php 
                                                    if($pincho["CELIACOS"]==0) { 
                                                        echo "No apto";
                                                    } else { 
                                                        echo "Apto";
                                                    }; 
                                                ?>
                                                </th>
                                                <th>
                                                    <form action="indexOrganizador.php?controller=pinchos&amp;action=validatePincho" method="POST">
                                                        <input type="text" class="form-control form-pers" style="display:none" name="nombrePincho" value="<?php echo $pincho["NOMBRE"]; ?>">
                                                        <input type="text" class="form-control form-pers" style="display:none" name="emailEst" value="<?php echo $pincho["EMAIL_EST"]; ?>">                                                                    
                                                        <input type="submit" class="btn btn-default btn-hover-green" value="Validar">
                                                    </form>
                                                </th>                                                              
                                            </tr>
                                        <?php 
                                            } 
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END VALIDAR PINCHO -->


            <!-- START ASIGNAR PINCHO -->
            <div id="index4" class="container-fluid"  <?php if($view!="index4"){ echo "style='display:none;'"; }?> >
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Asignar Pichos</h1>
                    </div>
                </div>        

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Lista de pinchos para asignar</div>
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>Pincho</th>
                                                <th>Email_establecimiento</th>
                                                <th>Ingredientes</th>
                                                <th>Celíacos</th>                                         
                                                <th>J Profesional</th>                                        
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $prueba = $_SESSION["__flasharray__"]["pinchosOrg"];
                                                $jPro = $_SESSION["__flasharray__"]["jPro"];                                        
                                                $numPinchos = count($prueba);
                                                foreach($prueba as $pincho) {
                                            ?>  
                                                <tr>
                                                    <th><?php 
                                                        echo $pincho["NOMBRE"]; 
                                                    ?>
                                                    </th>
                                                    <th>
                                                    <?php 
                                                        echo $pincho["EMAIL_EST"]; 
                                                    ?>
                                                    </th>
                                                    <th>
                                                    <?php 
                                                        echo $pincho["INGREDIENTES"]; 
                                                    ?>
                                                    </th>                                                          
                                                    <th>
                                                    <?php 
                                                        if($pincho["CELIACOS"]==0) { 
                                                            echo "No apto";
                                                        } else { 
                                                            echo "Apto";
                                                        }; 
                                                    ?>
                                                    </th>  
                                                    <form action="indexOrganizador.php?controller=pinchos&amp;action=assignPincho" method="POST">
                                                    <th>
                                                        <select class="form-control" name="jpro">
                                                           <?php foreach($jPro as $jp) { ?>
                                                            <option value="<?php echo $jp["EMAIL"]; ?>" ><?php echo $jp["EMAIL"]." | ".$jp["NOMBRE"]." | ".$jp["APELLIDOS"]; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </th>                                                  
                                                    <th>
                                                        <input type="text" class="form-control form-pers" style="display:none" name="nombrePincho" value="<?php echo $pincho["NOMBRE"]; ?>">
                                                        <input type="text" class="form-control form-pers" style="display:none" name="emailEst" value="<?php echo $pincho["EMAIL_EST"]; ?>">                                                                    
                                                        <input type="submit" class="btn btn-default btn-hover-green" value="Asignar">
                                                    </form>
                                                    </th>                                                              
                                                </tr>
                                            <?php 
                                                } 
                                            ?>
                                        </tbody>
                                    </table> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                  
           

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Lista de pinchos asignados</div>
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>Pincho</th>
                                                <th>Email_establecimiento</th>
                                                <th>Email_jurado_profesional</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $pAsigned = $_SESSION["__flasharray__"]["pinchosAsigned"];                                   
                                                $numPinchos = count($pAsigned);
                                                foreach($pAsigned as $pincho) {
                                            ?>  
                                                <tr>
                                                    <th><?php 
                                                        echo $pincho["NOMBRE_PINCHO"]; 
                                                    ?>
                                                    </th>
                                                    <th>
                                                    <?php 
                                                        echo $pincho["EMAIL_EST"]; 
                                                    ?>
                                                    </th>
                                                    <th>
                                                    <?php 
                                                        echo $pincho["EMAIL_USER"]; 
                                                    ?>
                                                    </th>                                                                
                                                </tr>
                                            <?php 
                                                } 
                                            ?>
                                        </tbody>
                                    </table> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ver/modificar jurado profesiones -->
        <div id="index5" class="container-fluid"  <?php if($view!="index5"){ echo "style='display:none;'"; }?> >
        <hr>
        <div id="tablaPinchos" class="row">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Alias</th>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Correo electrónico</th>
                        <th>Contraseña</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $num = 1;

                    foreach($jPro as $jpro) {
                ?>  
                    <tr>
                        <th><?php 
                            echo $jpro["USERNAME"]; 
                        ?>
                        </th>
                        <th><?php 
                            echo $jpro["NOMBRE"]; 
                        ?>
                        </th>                        
                        <th><?php 
                            echo $jpro["APELLIDOS"]; 
                        ?>
                        </th> 
                        <th>
                        <?php 
                            echo $jpro["EMAIL"]; 
                        ?>
                        </th>   
                        <th>
                        <?php 
                            echo $jpro["PASSWORD"]; 
                        ?>
                        </th>                         
                        <th>#
                        </th>                        
                        <th>
                            <a data-toggle="modal" data-target="<?php echo "#updt".$num; ?>" href="#" class="btn btn-primary" >Modificar</a>

                             <!-- START MODAL MODIFICAR PINCHO -->
                            <div id="<?php echo "updt".$num; ?>" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h3 class="text-center">Modificar Jurado Profesional</h3>
                                  </div>
                                    <form action="indexOrganizador.php?controller=users&amp;action=editJPro" method="POST">  
                                      <div class="modal-body"> 
                                      <h4>Username</h4>
                                        <input type="text" class="form-control form-pers" placeholder="Username" name="username" value="<?php echo $jpro["USERNAME"]; ?>"/>               
                                        <h4>Correo electrónico</h4>
                                        <input type="text" class="form-control form-pers" placeholder="Correo electrónico" name="emailMod" value="<?php echo $jpro["EMAIL"]; ?>"/>
                                        <input type="text" class="form-control form-pers" style="display:none;" placeholder="Correo electrónico" name="email" value="<?php echo $jpro["EMAIL"]; ?>"/>                                        
                                        <h4>Nombre</h4>                                       
                                        <input type="text" class="form-control form-pers" placeholder="Nombre" name="nombre" value="<?php echo $jpro["NOMBRE"]; ?>"/>   
                                        <h4>Apellidos</h4>
                                        <input type="text" class="form-control form-pers" placeholder="Apellido" name="apellidos" value="<?php echo $jpro["APELLIDOS"]; ?>"/>   
                                        <h4>Contraseña</h4>
                                        <input type="text" class="form-control form-pers" placeholder="Contraseña" name="pass" value="<?php echo $jpro["PASSWORD"]; ?>"/>
                                        <div class="clearfix"></div>
                                      </div>
                                      <div class="modal-footer">
                                        <a href="#" data-dismiss="modal" class="btn btn-primary">Cerrar</a>
                                        <input type="submit" class="btn btn-success" value="Guardar cambios"></a>
                                      </div>
                                    </form>  
                                </div>
                              </div>
                            </div> 
                            <!-- END MODAL MODIFICAR PINCHO --> 

                        </th>
                        <th>
                            <a data-toggle="modal" data-target="<?php echo "#del".$num; ?>" href="#" class="btn btn-danger">Eliminar</a>

                            <!-- START MODAL ELIMINAR PINCHO SUSTITUIR POR ALERTIFY-->
                            <div id="<?php echo "del".$num; ?>" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 class="text-center">Eliminar jurado profesional</h3>
                                  </div>
                                    <form action="indexOrganizador.php?controller=users&amp;action=removeJPro" method="POST"> 
                                      <div class="modal-body">                
                                        <h4 id="centrar">¿Seguro que quieres eliminar el pincho?</h4>
                                        <input type="text" class="form-control form-pers" style="display:none;" name="email" value="<?php echo $jpro["EMAIL"]; ?>"/>                             
                                        <div class="col-md-1 col-md-offset-4"><input type="submit" class="btn btn-success" value="Sí"></a></div>
                                        <div class="col-md-2 col-md-offset-1"><a href="indexOrganizador.php" data-dismiss="modal" class="btn btn-danger">No</a></div>
                                        <div class="clearfix"></div>
                                      </div>
                                    </form>                                                                      
                                  <div class="modal-footer">
                                    <a href="#" data-dismiss="modal" class="btn btn-primary">Cerrar</a>
                                  </div>
                                </div>
                              </div>
                            </div> 
                            <!-- END MODAL ELIMINAR PINCHO --> 

                        </th>
                    </tr>
                <?php $num += 1;
                    } 
                ?>
                </tbody>
            </table>
        </div>
    </div>




    <div id="index6" class="container-fluid"  <?php if($view!="index6"){ echo "style='display:none;'"; }?> >
        <div class="col-md-5">
            <div class="form-area">  
                <form action="indexOrganizador.php?controller=users&amp;action=saveJPro" method="POST">  
                    <br style="clear:both">
                    <h3 style="margin-bottom: 25px; text-align: center;">Creación de jurado profesional</h3>
                    <div class="form-group">
                        <input type="text" id="username" class="form-control" placeholder="Usarname" name="username">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Nombre" name="nombre">
                    </div>  
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Apellidos" name="apellidos">
                    </div>  
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email" name="email">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Password" name="pass">
                    </div>     
                    <input type="submit" class="btn btn-primary pull-right" value="Crear jurado profesional">
                </form>
            </div>
        </div>
    </div>  <!-- END CREAR CONCURSO -->






        </div> <!-- /#page-wrapper -->


    </div><!-- /#wrapper -->

   

    <!-- jQuery -->
    <script src="../../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../../js/jquery.easing.min.js"></script>

    <script src="../../js/toastr.js"></script>
    <script src="../../dist/jquery.flip.min.js"></script>
    <script>
      $(document).ready(function() {
        $('.card a').swipebox();
    $('.card').flip();
      });
    </script>




<?php if(isset($_GET["save"])) {
        if($_GET["save"]=="true"){ ?>
            <script language="javascript" charset="UTF-8">      
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-full-width",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };
                toastr.success("El concurso se ha registrado correctamente.", "Se ha añadido un nuevo concurso");
            </script>        
<?php   } 

        if($_GET["save"]=="false1"){
?>
            <script language="javascript">        
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-full-width",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };
                toastr.error("El concurso ya se encuentra registrado.", "Error en la creación de un nuevo concurso"); 
            </script>
<?php
        }

       if($_GET["save"]=="false2"){
?>
            <script language="javascript">        
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-full-width",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };
                toastr.error("El concurso no se ha registrado correctamente. Vuelva a intentarlo de nuevo.", "Error en la creación de un nuevo concurso"); 
            </script>
<?php
        }


    }
?>


</body>

    <!--MODALES-->
    <!-- line modal Edit-->
     <div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <form action="indexOrganizador.php?controller=users&amp;action=" method="POST">  
              <div class="modal-body">  

                <h4>Nombre</h4>
                <input type="text" class="form-control form-pers" style="display:none;" name="username"/>
                <input type="text" class="form-control form-pers" placeholder="Nombre del Organizador" name="nombre"/>
                <h4>Apellidos</h4>
                <input type="text" class="form-control form-pers" style="display:none;" name="tipo" value="<?php echo $_SESSION["currentTypeUser"] ?>" />
                <input type="text" class="form-control form-pers" placeholder="Apellidos del Organizador" name="apellidos"/>              
                <h4>Email</h4>
                <input type="email" class="form-control form-pers" placeholder="Email del Organizador" name="email"/>
                 <h4>Nueva Password</h4>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="pass">
                <h4>Confirmar password</h4>
                <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Confirm password" name="confpass">
                <div class="clearfix"></div>
              </div>
              <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn btn-primary">Cancelar</a>
                <input type="submit" class="btn btn-success" value="Guardar cambios"></a>
              </div>
            </form> 
            </div>
        </div>
    </div>


</html>
