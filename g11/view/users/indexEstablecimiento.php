<?php

define("DEFAULT_CONTROLLER", "establishments");

define("DEFAULT_ACTION", "showPinchosEst");


function run() {

  try {
    if (!isset($_GET["controller"])) {
      $_GET["controller"] = DEFAULT_CONTROLLER; 
    }
    
    if (!isset($_REQUEST["action"])) {
      $_GET["action"] = DEFAULT_ACTION;
    }
    $controller = loadController($_GET["controller"]);
    $actionName = $_GET["action"];
    $controller->$actionName(); 
  } catch(Exception $ex) {
    die("An exception occured!!!!!".$ex->getMessage());   
  }
}
 
function loadController($controllerName) {  
  $controllerClassName = getControllerClassName($controllerName);
  require_once(__DIR__."/../../controller/".$controllerClassName.".php");  
  return new $controllerClassName();
}
 
function getControllerClassName($controllerName) {
  return strToUpper(substr($controllerName, 0, 1)).substr($controllerName, 1)."Controller";
}
 
run();

if(!isset($_SESSION["currentEstablishment"])) {
 header('Location: ../../not_acceptable.html');
   
} ?>
            <!DOCTYPE html>
            <html lang="en">

            <head>

                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="description" content="">
                <meta name="author" content="">

                <title>Pinchito4u</title>


                <link href="../../css/bootstrap.min.css" rel="stylesheet">


<!-- Custom CSS -->
                <link href="../../css/sb-admin-2.css" rel="stylesheet">


                <!-- Custom Fonts -->
                <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
                <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
                <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">






                 <script>
            <?php define("DEFAULT_VIEW", "index2"); 
           
            if (!isset($_GET["view"])) {
                $view= DEFAULT_VIEW; 
            } else {
                $view=$_GET["view"];
            }
             ?>

            var visible = <?php echo "'".$view."';"; ?>
                    
                    function muestra_oculta(id){
                        
                        if (document.getElementById){ 
                            var hidden = document.getElementById(id);
                             
                            if(hidden != document.getElementById(visible)){
                                hidden.style.display = 'block';
                                document.getElementById(visible).style.display = 'none';  
                                switch(id){
                                    case 'index1':  visible='index1';
                                                    break;
                                    case 'index2':  visible='index2';
                                                    break;
                                    case 'index3':  visible='index3';
                                                    break;
                                }
                            }
                        }
                    }
                </script>              

            </head>

            <body>

                <div id="wrapper">

                             <!-- barra superior -->
                            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="indexEstablecimiento.php">Pinchito4u</a>
                                </div>
                                <!-- /.navbar-header -->

                                <ul class="nav navbar-top-links navbar-right">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION["dataEstablishment"]["NOMBRE"]; ?> <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                          <li><div class="center"><button data-toggle="modal" data-target="#squarespaceModal" class="btn btn-primary center-block">Editar</button></div></li>
                                          <li role="separator" class="divider"></li>
                                          <li>
                                           <form action="indexEstablecimiento.php?controller=Establishments&amp;action=removeEst" method="POST">
                                           <div class="center"><button class="btn btn-primary center-block">Eliminar cuenta</button></div>
                                           </form>   
                                          </li>
                                          <li role="separator" class="divider"></li>
                                            <li>
                                            <form action="indexEstablecimiento.php?controller=users&amp;action=logout" method="POST">
                                            <div class="center"><button class="btn btn-primary center-block">Cerrar Sesión</button></div>
                                            </form>
                                            </li>
                                        </ul>
                                      </li>                                   
                                </ul>
                        <!-- /.navbar-top-links -->

                        <!-- La barra lateral izquierda -->
                        <div class="navbar-default sidebar" role="navigation">
                            <div class="sidebar-nav navbar-collapse">
                                <ul class="nav" id="side-menu">
                                    <li>
                                        <a href="#" onclick="muestra_oculta('index1')"> Añadir Pincho</a>
                                        <a href="#" onclick="muestra_oculta('index2')"> Ver Pinchos</a>
                                        <a href="#" onclick="muestra_oculta('index3')"> Generar código</a>                                        
                                    </li>
                                   
                                </ul>
                            </div>
                            <!-- /.sidebar-collapse -->
                        </div>
                        <!-- /.navbar-static-side -->
                    </nav>


                    <div id="page-wrapper">
                           


        <div id="index2" class="container-fluid"  <?php if($view!="index2"){ echo "style='display:none;'"; }?> >
        <hr>
        <div id="tablaPinchos" class="row">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Precio</th>
                        <th>Ingredientes</th>
                        <th>Foto</th>
                        <th>Celiaco</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $prueba = $_SESSION["__flasharray__"]["pinchosEst"];
                    $numPinchos = count($prueba);

                    $validado=0;
                    foreach($prueba as $val) {
                        if($val["VALIDACION"]==1) {
                            $validado=1;
                            break;
                        }
                    }


                    foreach($prueba as $pincho) {
                ?>  
                    <tr>
                        <th><?php 
                            echo $pincho["NOMBRE"]; 
                        ?>
                        </th>
                        <th><?php 
                            echo $pincho["DESCRIPCION"]; 
                        ?>
                        </th>                        
                        <th><?php 
                            echo $pincho["PRECIO"]." €"; 
                        ?>
                        </th> 
                        <th>
                        <?php 
                            echo $pincho["INGREDIENTES"]; 
                        ?>
                        </th>
                        <th>
                        <?php 
                            echo $pincho["FOTO"]; 
                        ?>
                        </th>                        
                        <th>
                        <?php 
                            if($pincho["CELIACOS"]==0) { 
                                echo "No apto";
                            } else { 
                                echo "Apto";
                            }; 
                        ?>
                        </th>
                        <th>#
                        </th>                        
                        <th>
                            <?php $nombreModal = str_replace(" ", "", $pincho["NOMBRE"]) ?>
                            <a data-toggle="modal" data-target="<?php echo "#updt".$nombreModal; ?>" href="#" class="btn btn-primary" <?php if($validado==1) {echo "disabled";}?> >Modificar Pincho</a>

                             <!-- START MODAL MODIFICAR PINCHO -->
                            <div id="<?php echo "updt".$nombreModal; ?>" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h3 class="text-center">Modificar Pincho</h3>
                                  </div>
                                    <form action="indexEstablecimiento.php?controller=establishments&amp;action=editPincho" method="POST">  
                                      <div class="modal-body">                
                                        <h4>Nombre del Pincho</h4>
                                        <input type="text" class="form-control form-pers" placeholder="Nombre del Pincho" name="nombreMod" value="<?php echo $pincho["NOMBRE"]; ?>"/>
                                        <input type="text" class="form-control form-pers" style="display:none;" placeholder="Nombre del Pincho" name="nombre" value="<?php echo $pincho["NOMBRE"]; ?>"/>                                        
                                        <input type="text" class="form-control form-pers" style="display:none;" name="email_est" value="<?php echo $_SESSION["currentEstablishment"]; ?>"/>   
                                        <h4>Descripcion</h4>
                                        <textarea id="descripcion" class="form-control txtarea-mod" placeholder="Descripción" rows="3" name="descripcion"><?php echo $pincho["DESCRIPCION"]; ?></textarea>
                                        <h4>Precio</h4>
                                        <input type="number" step="0.01" class="form-control form-pers" placeholder="Precio" name="precio" value="<?php echo $pincho["PRECIO"]; ?>"/>
                                        <h4>Ingredientes</h4>
                                        <textarea id="ingredientes" placeholder="Ingredientes separados por espacios" class="form-control txtarea-mod" rows="3" name="ingredientes"><?php echo $pincho["INGREDIENTES"]; ?></textarea>
                                        <h4>Celiaco</h4>
                                        <select class="form-control" name="celiacos">
                                            <option value="0">No apto</option>
                                            <option value="1">Apto</option>
                                        </select>
                                        <h4>Fotos</h4>
                                        <div class="form-group">
                                            <input type="file" name="foto" id="archivo"></input>
                                        </div>
                                        <div class="clearfix"></div>
                                      </div>
                                      <div class="modal-footer">
                                        <a href="#" data-dismiss="modal" class="btn btn-primary">Cerrar</a>
                                        <input type="submit" class="btn btn-success" value="Guardar cambios"></a>
                                      </div>
                                    </form>  
                                </div>
                              </div>
                            </div> 
                            <!-- END MODAL MODIFICAR PINCHO --> 

                        </th>
                        <th>
                            <a data-toggle="modal" data-target="<?php echo "#del".$nombreModal; ?>" href="#" class="btn btn-danger" <?php if($validado==1) {echo "disabled";}?> >Eliminar Pincho</a>

                            <!-- START MODAL ELIMINAR PINCHO SUSTITUIR POR ALERTIFY-->
                            <div id="<?php echo "del".$nombreModal; ?>" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 class="text-center">Eliminar Pincho</h3>
                                  </div>
                                    <form action="indexEstablecimiento.php?controller=establishments&amp;action=delPincho" method="POST"> 
                                      <div class="modal-body">                
                                        <h4 id="centrar">¿Seguro que quieres eliminar el pincho?</h4>
                                        <input type="text" class="form-control form-pers" style="display:none;" name="email_est" value="<?php echo $_SESSION["currentEstablishment"]; ?>"/>                
                                        <input type="text" class="form-control form-pers" style="display:none;" name="nombre" value="<?php echo $pincho["NOMBRE"]; ?>"/>                
                                        <div class="col-md-1 col-md-offset-4"><input type="submit" class="btn btn-success" value="Sí"></a></div>
                                        <div class="col-md-2 col-md-offset-1"><a href="indexEstablecimiento.php" data-dismiss="modal" class="btn btn-danger">No</a></div>
                                        <div class="clearfix"></div>
                                      </div>
                                    </form>                                                                      
                                  <div class="modal-footer">
                                    <a href="#" data-dismiss="modal" class="btn btn-primary">Cerrar</a>
                                  </div>
                                </div>
                              </div>
                            </div> 
                            <!-- END MODAL ELIMINAR PINCHO --> 

                        </th>
                        <th>
                            <form action="indexEstablecimiento.php?controller=establishments&amp;action=participate" method="POST"> 
                                <input class="form-control" style="display:none;" type="text" name="nombrePincho" value="<?php echo $pincho['NOMBRE']; ?>" >
                                <input class="form-control" style="display:none;" type="text" name="emailEst" value="<?php echo $pincho['EMAIL_EST']; ?>" >
                                <input type="submit" class="btn btn-success" value="Participar" <?php if($validado==1) {echo "disabled";}?>  ></input>
                            </form>
                        </th>

                    </tr>
                <?php 
                    } 
                ?>
                </tbody>
            </table>
        </div>
    </div>





    <div id="index1" class="container-fluid" <?php if($view!="index1"){ echo "style='display:none;'"; }?>>
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 class="text-center">Añadir Pincho</h3>
          </div>
            <form action="indexEstablecimiento.php?controller=establishments&amp;action=addPincho" method="POST">          
              <div class="modal-body">                
                <h4>Nombre del Pincho</h4>
                <input type="text" class="form-control form-pers" placeholder="Nombre del Pincho" name="nombre"/>
                <input type="text" class="form-control form-pers" style="display:none;" name="email_est" value="<?php echo $_SESSION["currentEstablishment"]; ?>"/>                
                <h4>Descripcion</h4>
                <textarea id="descripcion" class="form-control txtarea-mod" placeholder="Descripción" rows="3" name="descripcion"></textarea>
                <h4>Precio</h4>
                <input type="number" step="0.01" class="form-control form-pers" placeholder="Precio" name="precio"/>
                <h4>Ingredientes</h4>
                <textarea placeholder="Ingredientes separados por comas" class="form-control txtarea-mod" id="ingredientes" rows="3" name="ingredientes"></textarea>
                <h4>Celiaco</h4>
                <select class="form-control" name="celiacos">
                    <option value="0">No apto</option>
                    <option value="1">Apto</option>
                </select>
                <h4>Fotos</h4>
                <div class="form-group">
                    <input type="file" name="foto" id="archivo"></input>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="modal-footer">
                <input type="submit" class="btn btn-success" value="Añadir" <?php if($validado==1) {echo "disabled";}?> ></a>
              </div>
            </form>                        
        </div>
      </div>
    </div>


                <div id="index3" class="container-fluid" <?php if($view!="index3"){ echo "style='display:none;'"; }?>>
                            

                    <h2 id="jPop" style="text-align:center; border-bottom: 1px solid black;">Generar código</h2>          


                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>¡Generar código!</strong> Genere nuevos códigos para sus pinchos.
                    </div>      


                    <div class="row">
                        <div class="col-lg-offset-3 col-lg-6">
                            <form action="indexEstablecimiento.php?controller=establishments&amp;action=requestCode" method="POST">    
                                <button class="btn btn-success" type="submit" <?php if($validado==0) {echo "disabled";}?> >Generar</button>
                            </form>
                        </div>
                    </div>

                    <h2 id="jPop" style="text-align:center; border-bottom: 1px solid black;">Código generado</h2>
                    

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Código</th>                      
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php if(!empty($_SESSION["__flasharray__"]["codeGenerator"])){ ?>
                                <td> <?php echo "1"; ?> </td>
                                <td> <?php  echo $_SESSION["__flasharray__"]["codeGenerator"]; ?> </td>  
                                <?php } ?>                                                         
                            </tr>
                        </tbody>
                    </table>




                        </div>
                   </div>
                    <!-- /#page-wrapper -->

                </div>
                <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../../js/jquery.easing.min.js"></script>


            </body>


                <!--modales-->
                <!-- line modal Edit-->
    <!--MODALES-->
    <!-- line modal Edit-->
    <div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <form action="indexEstablecimiento.php?controller=Establishments&amp;action=editEst" method="POST">  
              <div class="modal-body">  

                <h4>CIF</h4>
                <input type="text" class="form-control form-pers" placeholder="CIF" value="<?php echo $_SESSION["dataEstablishment"]["CIF"]; ?>" name="cif"/>
                <h4>Nombre</h4>
                <input type="text" class="form-control form-pers" placeholder="Nombre" value="<?php echo $_SESSION["dataEstablishment"]["NOMBRE"]; ?>"  name="nombre_est"  /> 
                <h4>Email</h4>
                <input type="email" class="form-control form-pers" placeholder="Correo electrónico" value ="<?php echo $_SESSION["currentEstablishment"]; ?>" name="email_est" disabled/>            
                <h4>Localización</h4>
                <input type="texto" class="form-control form-pers" placeholder="Localización" value="<?php echo $_SESSION["dataEstablishment"]["LOCALIZACION"]; ?>" name="localizacion" />
                 <h4>Nueva Password</h4>
                <input type="password" class="form-control" placeholder="Contraseña" name="password">
                <h4>Confirmar password</h4>
                <input type="password" class="form-control"  placeholder="Confirmar contraseña" name="confpassword">
                <div class="clearfix"></div>
              </div>
              <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn btn-primary">Cancelar</a>
                <input type="submit" class="btn btn-success" value="Guardar cambios"></a>
              </div>
            </form> 
            </div>
        </div>
    </div>
            </div>




</html>




