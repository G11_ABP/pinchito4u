<?php
 
define("DEFAULT_CONTROLLER", "pinchos");
 
define("DEFAULT_ACTION", "showPinchosJPro");

 
function run() { 
  try {
    if (!isset($_GET["controller"])) {
      $_GET["controller"] = DEFAULT_CONTROLLER; 
    }
    
    if (!isset($_REQUEST["action"])) {
      $_GET["action"] = DEFAULT_ACTION;
    }
 
    $controller = loadController($_GET["controller"]);

    $actionName = $_GET["action"];
    $controller->$actionName(); 
  } catch(Exception $ex) {
    die("An exception occured!!!!!".$ex->getMessage());   
  }
}
 
function loadController($controllerName) {  
  $controllerClassName = getControllerClassName($controllerName);
  require_once(__DIR__."/../../controller/".$controllerClassName.".php");  
  return new $controllerClassName();
}
 
function getControllerClassName($controllerName) {
  return strToUpper(substr($controllerName, 0, 1)).substr($controllerName, 1)."Controller";
}
 
run();

if($_SESSION["currentTypeUser"] != "JURADO PROFESIONAL") {
 header('Location: ../../not_acceptable.html');
   
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Jurado Profesional</title>

        <link href="../../css/bootstrap.min.css" rel="stylesheet">
        <link href="../../css/sb-admin-2.css" rel="stylesheet">


        <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">


</head>

<body>


        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="indexJProfesional.php">Pinchito4u</a>
            </div>
        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION["datacurrent"]["NOMBRE"]; ?><span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><div class="center"><button data-toggle="modal" data-target="#squarespaceModal" class="btn btn-primary center-block">Editar</button></div></li>
                  <li role="separator" class="divider"></li>
                  <li>
                   <form action="indexJProfesional.php?controller=users&amp;action=removeUser" method="POST">
                   <div class="center"><button class="btn btn-primary center-block">Eliminar cuenta</button></div>
                   </form>   
                  </li>
                  <li role="separator" class="divider"></li>
                    <li>
                    <form action="indexJProfesional.php?controller=users&amp;action=logout" method="POST">
                    <div class="center"><button class="btn btn-primary center-block">Cerrar Sesión</button></div>
                    </form>
                    </li>
                </ul>
              </li>                                   
        </ul>
        
        </nav>  <!-- END NAVBAR -->




    <!-- Page Content -->
    <div class="container">


            <div>
                <h2 id="jPro" style="text-align:center; border-bottom: 1px solid black;">Pinchos</h2>   
            </div>
            <?php
                $prueba = $_SESSION["__flasharray__"]["pinchosJPro"];
                foreach($prueba as $pincho) {
            ?>    


        <!-- Page Features -->
        <div class="row text-center">
            
            <div class="col-md-4 col-sm-8 hero-feature">
                <div class="thumbnail">
                    <img src="../../img/pincho-huevo.jpg" alt="Pincho Huevo">
                    <div class="caption">
                        <h3><?php echo $pincho['NOMBRE'] ?></h3>
                        <p><?php echo $pincho['DESCRIPCION'] ?> </p>
                        <form action="indexJProfesional.php?controller=pinchos&amp;action=voteJPro" method="POST">
                            <div class="col-md-4 col-sm-2 hero-feature">
                                <input class="form-control" style="display:none;" type="text" name="pinchoNombre" value="<?php echo $pincho['NOMBRE']; ?>" >
                                <input class="form-control" style="display:none;" type="text" name="emailEst" value="<?php echo $pincho['EMAIL_EST']; ?>" >                                   
                                <select class="form-control" name="valoracion">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div>
                                <input type="submit" class="btn btn-success" value="Votar"

                                <?php 
                                $votes = $_SESSION["__flasharray__"]["valuesJPro"];

                                foreach ($votes as $vote) { 
                                    if($vote['NOMBRE_PINCHO'] == $pincho['NOMBRE'] && $vote['EMAIL_EST']==$pincho['EMAIL_EST'] ) {
                                        echo "disabled";
                                    }
                                }

                                ?>
                                >   
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php } ?>  
        </div>
        <!-- /.row -->


        <h2 id="jPro" style="text-align:center; border-bottom: 1px solid black;">Valoraciones</h2>
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Pinchos valorados.</strong> Aquí podrá ver los pinchos y sus puntuaciones.
        </div>                    

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Valoración</th>
                    <th>Pincho</th>  
                    <th>Establecimiento</th>                                                                
                </tr>
            </thead>
            <tbody>
            
            <?php 
                $codes = $_SESSION["__flasharray__"]["valuesJPro"];
                                            $nCode = 1;
                foreach($codes as $c) {
            ?> 
            
            <tr>
                <td> <?php echo $nCode; ?> </td>
                <td> <?php echo $c["VALORACION"]; ?> </td>
                <td> <?php echo $c["NOMBRE_PINCHO"]; ?> </td>   
                <td> <?php echo $c["NOMBRE_EST"]; ?> </td>                                                                
            </tr>
            
            <?php 
                                              $nCode += 1;   } 
            ?>
            
            </tbody>
        </table>



    </div>


        <script src="../../js/jquery.js"></script>
        <script src="../../js/bootstrap.min.js"></script>

</body>


                <!--modales-->
                <!-- line modal Edit-->
    <div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <form action="indexJProfesional.php?controller=users&amp;action=editUser" method="POST">  
              <div class="modal-body">  
                <h4>Nombre</h4>
                <input type="text" class="form-control form-pers" style="display:none" placeholder="Username" value ="<?php echo $_SESSION["datacurrent"]["USERNAME"]; ?>" name="username" />
                <input type="text" class="form-control form-pers" placeholder="Nombre" value ="<?php echo $_SESSION["datacurrent"]["NOMBRE"]; ?>" name="nombre"/>
                <h4>Apellidos</h4>
                <input type="text" class="form-control form-pers" placeholder="Apellidos" value ="<?php echo $_SESSION["datacurrent"]["APELLIDOS"]; ?>" name="apellidos"/>              
                <h4>Email</h4>
                <input type="email" class="form-control form-pers" placeholder="Email" value ="<?php echo $_SESSION["datacurrent"]["EMAIL"]; ?>" name="email" disabled/>
                 <h4>Nueva Password</h4>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="pass">
                <h4>Confirmar password</h4>
                <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Confirm password" name="confpassword">
              <div class="clearfix"></div>
              </div>
              <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn btn-primary">Cancelar</a>
                <input type="submit" class="btn btn-success" value="Guardar cambios"></a>
              </div>
            </form> 
            </div>
        </div>
    </div>

</html>
