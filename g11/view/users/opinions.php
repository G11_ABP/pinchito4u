<?php

    define("DEFAULT_CONTROLLER", "pinchos");

    define("DEFAULT_ACTION", "opinions");


    function run() {

        try {
            if (!isset($_GET["controller"])) {
                $_GET["controller"] = DEFAULT_CONTROLLER; 
            }

            if (!isset($_REQUEST["action"])) {
                $_GET["action"] = DEFAULT_ACTION;
            }

            $controller = loadController($_GET["controller"]);

            $actionName = $_GET["action"];
            $estName = $_GET["est"];
            $pinchoName = $_GET["pincho"];

            $controller->$actionName($estName, $pinchoName); 
        } catch(Exception $ex) {
            die("An exception occured!!!!!".$ex->getMessage());   
        }
    }

    function loadController($controllerName) {  
        $controllerClassName = getControllerClassName($controllerName);
        require_once(__DIR__."/../../controller/".$controllerClassName.".php");  
        
        return new $controllerClassName();
    }

    function getControllerClassName($controllerName) {
        return strToUpper(substr($controllerName, 0, 1)).substr($controllerName, 1)."Controller";
    }

    run();

    if($_SESSION["currentTypeUser"] != "JURADO POPULAR") {
        header('Location: ../../not_acceptable.html');   
    }

?>



<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Pinchito4u - Opiniones</title>

        <link href="../../css/bootstrap.min.css" rel="stylesheet">
        <link href="../../css/sb-admin-2.css" rel="stylesheet">


        <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

         <script>
          function showAnswer(id) {
            if(document.getElementById){
              var hidden = document.getElementById(id);
              $(hidden).fadeIn(1000);
            }
          }

          function hideAnswer(id) {
            if(document.getElementById){
              var visible = document.getElementById(id);
              $(visible).fadeOut(500);
            }
          }          
        </script>

    </head>

    <body>

        <div id="wrapper">

            <!-- barra superior -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="indexJPopular.php">Pinchito4u</a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <?= $_SESSION["datacurrent"]["NOMBRE"] ?>  <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><div class="center"><button data-toggle="modal" data-target="#squarespaceModal" class="btn btn-primary center-block">Editar</button></div></li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <form action="indexJPopular.php?controller=users&amp;action=removeUser" method="POST">
                                    <div class="center"><button class="btn btn-primary center-block">Eliminar cuenta</button></div>
                                </form>   
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <form action="indexJPopular.php?controller=users&amp;action=logout" method="POST">
                                    <div class="center"><button class="btn btn-primary center-block">Cerrar Sesión</button></div>
                                </form>
                            </li>
                        </ul>
                    </li>                                   
                </ul>
            </nav><!-- END MENÚ -->

                
      <div id="index1" class="container" style="background-color:white" >   
          
          <div class="row pincho">
          
            <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="row">
              <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
              <div class="thumbnail" style="border:none">
                  <img src="../../img/pincho.jpg" alt="pincho" >                  
                  <p class="margin-none" style="text-align:center"><?php echo $_GET['pincho']; ?></p>
               </div>
                </div>
                <div class="col-xs-8 col-sm-7 col-md-7 col-lg-7 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
                  <h1>Descripción</h1>
                  <p  style="font-size:20px">descripcion pincho</p>    
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="row">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                  <div class="answer">
                  <input class="btn btn-success" onclick="showAnswer('res')" value="Añadir comentario" type="button">
                </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    </div>
              </div>
            </div>
          </div>

          <hr class="h-divider marginTop-none">

          <div id="res" class="row add-opinion" style="display:none; padding:15px 10px; background-color:#EBE8EB;">
            <div class="row"> 
              <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
                <button type="button" class="close" onclick="hideAnswer('res')"><span aria-hidden="true">×</span></button>

                <form action="opinions.php?controller=pinchos&amp;action=addOpinion" method="POST">
                  <div style="display:none;"><input name="_method" value="POST" type="hidden"></div>
                  <input name="est" value="<?php echo $_GET['est']; ?>" type="hidden">
                  <input name="pincho" value="<?php echo $_GET['pincho']; ?>" type="hidden">           
                   <div class="form-group">
                    <label for="email">Comentario:</label>
                    <textarea name="opinion" class="form-control" rows="9"></textarea>               
                   </div>  
                  <button type="button" class="btn btn-info" onclick="hideAnswer('res')">Cerrar</button>
                  <input class="btn btn-success" value="Enviar" type="submit">
                </form>              

                </div>
            </div>
          </div>

          <hr class="h-divider">

          <?php
              $opinions = $_SESSION["__flasharray__"]["opinions"];
              foreach($opinions as $op) {
          ?>  
          <div class="row opinion">
            <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="row">
                 
                  <h4><?php echo $op['COMENTARIO_POPULAR']; ?></h4> 
                    <p title="num">Comentado por: <?= $op['EMAIL_USER']; ?></p>
                            </div>
            </div>

          </div>

          <hr class="h-divider">
          <?php } ?>

        </div><!-- END INDEX1 -->

               

        </div><!-- END WRAPPER -->



        <script src="../../js/jquery.js"></script>
        <script src="../../js/bootstrap.min.js"></script>

    </body>

      <div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <form action="indexJPopular.php?controller=users&amp;action=editUser" method="POST">  
              <div class="modal-body">  
                <h4>Nombre</h4>
                <input type="text" class="form-control form-pers" style="display:none" placeholder="Username" value ="<?php echo $_SESSION["datacurrent"]["USERNAME"]; ?>" name="username" />
                <input type="text" class="form-control form-pers" placeholder="Nombre" value ="<?php echo $_SESSION["datacurrent"]["NOMBRE"]; ?>" name="nombre"/>
                <h4>Apellidos</h4>
                <input type="text" class="form-control form-pers" placeholder="Apellidos" value ="<?php echo $_SESSION["datacurrent"]["APELLIDOS"]; ?>" name="apellidos"/>              
                <h4>Email</h4>
                <input type="email" class="form-control form-pers" placeholder="Email" value ="<?php echo $_SESSION["datacurrent"]["EMAIL"]; ?>" name="email" disabled/>
                 <h4>Nueva Password</h4>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="pass">
                <h4>Confirmar password</h4>
                <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Confirm password" name="confpassword">
              <div class="clearfix"></div>
              </div>
              <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn btn-primary">Cancelar</a>
                <input type="submit" class="btn btn-success" value="Guardar cambios"></a>
              </div>
            </form> 
            </div>
        </div>
    </div>


</html>
