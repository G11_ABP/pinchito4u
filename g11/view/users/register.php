


<!-- modificar  action form-->
<form action="index.php?controller=users&amp;action=register" method="POST" role="form">
    <div class="form-group" style="float:left; width: 147px; margin-right:10px;">
        <input id="nombre" class="form-control" type="text" placeholder="Nombre" name="user_name"/>
    </div>
    <div class="form-group" style="float:left; width: 287px;">
        <input id="apellidos" class="form-control" type="text" placeholder="Apellidos" name="user_lastname"/>
    </div>
    <div class="form-group" style="clear:both;">
        <input id="email" class="form-control" type="email" placeholder="Correo electrónico" name="user_email"/>
    </div>
    <div class="form-group"> 
        <input id="pwd" class="form-control" type="password" placeholder="Contraseña" name="user_passwd"/>
    </div>
    <div class="checkbox" style="display:inline; float:left;">
        <label><input type="checkbox"> Recuerdar mis datos</label>
    </div>
    <div style="display:inline; float:right; margin-top:10px; margin-bottom:10px;">
        <a href="#register" onclick="mostrar('recoverPass')">¿Has olvidado tu contraseña?</a>
    </div>
    <div class="container-fluid" style="clear:both">
        <button type="submit" class="btn btn-default">Registrarse</button>
    </div>
</form>