<?php

    define("DEFAULT_CONTROLLER", "pinchos");

    define("DEFAULT_ACTION", "showPinchosJPop");


    function run() {

        try {
            if (!isset($_GET["controller"])) {
                $_GET["controller"] = DEFAULT_CONTROLLER; 
            }

            if (!isset($_REQUEST["action"])) {
                $_GET["action"] = DEFAULT_ACTION;
            }

            $controller = loadController($_GET["controller"]);

            $actionName = $_GET["action"];
            $controller->$actionName(); 
        } catch(Exception $ex) {
            die("An exception occured!!!!!".$ex->getMessage());   
        }
    }

    function loadController($controllerName) {  
        $controllerClassName = getControllerClassName($controllerName);
        require_once(__DIR__."/../../controller/".$controllerClassName.".php");  
        
        return new $controllerClassName();
    }

    function getControllerClassName($controllerName) {
        return strToUpper(substr($controllerName, 0, 1)).substr($controllerName, 1)."Controller";
    }

    run();

    if($_SESSION["currentTypeUser"] != "JURADO POPULAR") {
        header('Location: ../../not_acceptable.html');   
    }

?>



<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Pinchito4u</title>

        <link href="../../css/bootstrap.min.css" rel="stylesheet">
        <link href="../../css/sb-admin-2.css" rel="stylesheet">


        <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">



        <script>

            <?php define("DEFAULT_VIEW", "index1"); 
           
            if (!isset($_GET["view"])) {
                $view= DEFAULT_VIEW; 
            } else {
                $view=$_GET["view"];
            }
             ?>

            var visible = <?php echo "'".$view."';"; ?>
            
            function muestra_oculta(id) {               
                if (document.getElementById) { 
                    var hidden = document.getElementById(id);                     
                    if(hidden != document.getElementById(visible)) {
                        hidden.style.display = 'block';
                        document.getElementById(visible).style.display = 'none';  
                        switch(id) {
                            case 'index1':  visible='index1';
                                            break;
                            case 'index2':  visible='index2';
                                            break;
                        }
                    }
                }
            }
        </script>

    </head>

    <body>

        <div id="wrapper">

            <!-- barra superior -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="indexJPopular.php">Pinchito4u</a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <?= $_SESSION["datacurrent"]["NOMBRE"] ?>  <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><div class="center"><button data-toggle="modal" data-target="#squarespaceModal" class="btn btn-primary center-block">Editar</button></div></li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <form action="indexJPopular.php?controller=users&amp;action=removeUser" method="POST">
                                    <div class="center"><button class="btn btn-primary center-block">Eliminar cuenta</button></div>
                                </form>   
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <form action="indexJPopular.php?controller=users&amp;action=logout" method="POST">
                                    <div class="center"><button class="btn btn-primary center-block">Cerrar Sesión</button></div>
                                </form>
                            </li>
                        </ul>
                    </li>                                   
                </ul>
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                                <a href="#" onclick="submitForm('showPinchos');muestra_oculta('index1')"> Votacion popular</a>
                                <a href="#" onclick="submitForm('showCodes');muestra_oculta('index2')"> Introducir código</a>                                       
                            </li>
                        </ul>
                    </div>
                </div>
            </nav><!-- END MENÚ -->


        <script>            
            function submitForm(id) {
                if(id=='showCodes'){               
                    location.href="indexJPopular.php?controller=codes&action=showCodesUser";
                }
                if(id=='showPinchos'){               
                    location.href="indexJPopular.php?controller=pinchos&action=showPinchosJPop";
                }
            }
        </script>



            <div id="page-wrapper">
                
                <div id="index1" class="container-fluid" <?php if($view=="index2"){ echo "style='display:none;'"; }?> >   

                    <div>
                        <h2 id="jPop">Pinchos con código</h2>
                    </div>


                    <?php

                    if($_SESSION['__flasharray__']['votesJPop'] == 1) { ?>  
                        <div class="alert alert-warning">
                            <strong>¡Gracias por su voto!</strong> 
                        </div>     
                    <?php } else { 
                            if($_SESSION['__flasharray__']['nCodesJPop'] == 0) {
                    ?>         
                        <div class="alert alert-success">
                            <strong>¡Vota ya!</strong> Ahora ya puede votar a su pincho favorito.
                        </div> 
                    <?php } else {  ?>
                    <div class="alert alert-warning">
                        <strong>¡Faltan <?php echo $_SESSION['__flasharray__']['nCodesJPop']; ?> voto/s!</strong> Todavía no puede votar. 
                    </div>

                    <?php }} ?>

                    <?php
                        $pinchos = $_SESSION["__flasharray__"]["pinchosJPop"];
                        $numPinchos = count($pinchos);
                        foreach($pinchos as $pincho) {
                    ?>    
                    <div class="col-md-4 col-sm-8 hero-feature">
                        <div class="thumbnail">
                            <a href="opinions.php?est=<?php echo $pincho['EMAIL_EST']; ?>&amp;pincho=<?php echo $pincho['NOMBRE']; ?>"  target="_blank" class="btn btn-info" style="float:right">+</a>
                            <img src="../../img/pincho.jpg" alt="Pincho con codigo">
                            <div class="caption">
                                <h3><?php echo $pincho['NOMBRE']; ?></h3>
                                <p><?php echo $pincho['DESCRIPCION']; ?></p>
                                <div>
                                    <div>
                                        <form action="indexJPopular.php?controller=pinchos&amp;action=voteJPop" method="POST">
                                            <input class="form-control" style="display:none;" type="text" name="nombrePincho" value="<?php echo $pincho['NOMBRE']; ?>" >
                                            <input class="form-control" style="display:none;" type="text" name="emailEst" value="<?php echo $pincho['EMAIL_EST']; ?>" >
                                            <input type="submit" class="btn btn-success" <?php  if($numPinchos != 3 || $_SESSION["__flasharray__"]["votesJPop"] != 0){ echo "disabled"; } ?> value="Votar" >                                    
                                        </form>          
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                        }

                    if($numPinchos < 3) {
                        $aux = 3 - $numPinchos;
                        
                        for ($i = 0; $i < $aux; $i++) { 
                    ?>

                    <div class="col-md-4 col-sm-8 hero-feature">
                        <div class="thumbnail">
                            <a href="" class="btn btn-info" style="float:right; visibility: hidden;">+</a>
                            <img src="../../img/pinchobyn.jpg" alt="Pincho sin codigo">
                            <div class="caption">
                                <h3>Introduce código</h3>
                                <p>Descripción introduce código</p>
                                <div>
                                    <div>
                                        <a href="#" class="btn btn-success" disabled>Votar</a>   
                                    </div>       
                                </div>
                            </div>
                        </div>
                    </div>
        
                    <?php
                            }
                        }        
                    ?>

                </div><!-- END INDEX1 -->

                <div id="index2" class="container-fluid" <?php if($view=="index1"){ echo "style='display:none;'"; }?>>
                    <h2 id="jPop" style="text-align:center; border-bottom: 1px solid black;">Introducir código</h2>          

                    <?php if($_SESSION['__flasharray__']['nCodesJPop'] == 0) { ?>  

                    <div class="alert alert-success">
                        <strong>¡Gracias por participar!</strong> Ahora ya puede votar a sus pinchos.
                    </div>      

                    <?php } else { ?>         

                    <div class="alert alert-warning">
                        <strong>Introduzca un código válido.</strong> Todavía puede introducir <strong><?php echo $_SESSION['__flasharray__']['nCodesJPop']; ?></strong> más.
                    </div>

                    <?php } ?>

                    <div class="row">
                        <div class="col-lg-offset-3 col-lg-6">
                            <form action="indexJPopular.php?controller=codes&amp;action=insertCode" method="POST">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Introduce codigo" name="code_ID" <?php if($_SESSION['__flasharray__']['nCodesJPop'] == 0) { echo "disabled"; } ?> >
                                    <span class="input-group-btn">
                                        <button class="btn btn-success" type="submit" <?php if($_SESSION['__flasharray__']['nCodesJPop'] == 0) { echo "disabled"; } ?>>Enviar</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>

                    <h2 id="jPop" style="text-align:center; border-bottom: 1px solid black;">Códigos</h2>
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Códigos introducidos.</strong> Aquí podrá ver los códigos introducidos.
                    </div>                    

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Código</th>
                                <th>Pincho</th>  
                                <th>Establecimiento</th>                                                                
                            </tr>
                        </thead>
                        <tbody>
                        
                        <?php 
                            $codes = $_SESSION["__flasharray__"]["codesJPop"];
                            $nCode = 1;
                            foreach($codes as $c) {
                        ?> 
                        
                        <tr>
                            <td> <?php echo $nCode; ?> </td>
                            <td> <?php echo $c["CODIGO"]; ?> </td>
                            <td> <?php echo $c["NOMBRE_PINCHO"]; ?> </td>   
                            <td> <?php echo $c["NOMBRE_EST"]; ?> </td>                                                                
                        </tr>
                        
                        <?php 
                                $nCode += 1; 
                            } 
                        ?>
                        
                        </tbody>
                    </table>

                </div><!-- END INDEX2 -->

            </div><!-- END PAGE-WRAPPER -->
        </div><!-- END WRAPPER -->



        <script src="../../js/jquery.js"></script>
        <script src="../../js/bootstrap.min.js"></script>

    </body>

      <div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <form action="indexJPopular.php?controller=users&amp;action=editUser" method="POST">  
              <div class="modal-body">  
                <h4>Nombre</h4>
                <input type="text" class="form-control form-pers" style="display:none" placeholder="Username" value ="<?php echo $_SESSION["datacurrent"]["USERNAME"]; ?>" name="username" />
                <input type="text" class="form-control form-pers" placeholder="Nombre" value ="<?php echo $_SESSION["datacurrent"]["NOMBRE"]; ?>" name="nombre"/>
                <h4>Apellidos</h4>
                <input type="text" class="form-control form-pers" placeholder="Apellidos" value ="<?php echo $_SESSION["datacurrent"]["APELLIDOS"]; ?>" name="apellidos"/>              
                <h4>Email</h4>
                <input type="email" class="form-control form-pers" placeholder="Email" value ="<?php echo $_SESSION["datacurrent"]["EMAIL"]; ?>" name="email" disabled/>
                 <h4>Nueva Password</h4>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="pass">
                <h4>Confirmar password</h4>
                <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Confirm password" name="confpassword">
              <div class="clearfix"></div>
              </div>
              <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn btn-primary">Cancelar</a>
                <input type="submit" class="btn btn-success" value="Guardar cambios"></a>
              </div>
            </form> 
            </div>
        </div>
    </div>


</html>
