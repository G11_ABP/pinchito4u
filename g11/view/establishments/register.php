

<form action="index.php?controller=establishments&amp;action=register" method="POST" role="form">
    <div class="form-group" style="float:left; width: 147px; margin-right:10px;">
        <input id="cif" class="form-control" type="text" placeholder="CIF" name="establishmentCIF"/>
    </div>
    <div class="form-group" style="float:left; width: 287px;">
        <input id="nombre" class="form-control" type="text" placeholder="Nombre" name="establishment_name"/>
    </div>
    <div class="form-group" style="clear:both;">
        <input id="localizacion" class="form-control" type="text" placeholder="Localización" name="establishment_location"/>
    </div>
    <div class="form-group">
        <input id="email" class="form-control" type="email" placeholder="Correo electrónico" name="establishment_email"/>
    </div>
    <div class="form-group"> 
        <input id="pwd" type="password" class="form-control" placeholder="Contraseña" name="establishment_passwd"/>
    </div>
    <div class="checkbox" style="display:inline; float:left;">
        <label><input type="checkbox"> Recuerdar mis datos</label>
    </div>
    <div style="display:inline; float:right; margin-top:10px; margin-bottom:10px;">
        <a href="#register" onclick="mostrar('recoverPass')">¿Has olvidado tu contraseña?</a>
    </div>
    <div class="container-fluid" style="clear:both">
        <button type="submit" class="btn btn-default">Registrar</button>
    </div>
</form> 
