INSERT INTO `PINCHITO4U` (`NOMBRE`, `LOGO`, `FECHA_INI`, `FECHA_FIN`, `BASES`, `PREMIO_PRO`, `PREMIO_POP`, `FOLLETO`) 
VALUES ('Certamen de Pinchos OU15', NULL, '20151127', '20151206', 'Juego limpio.', '1º 500€, 2º 300€, 3º 200€', '1º 150€, 2º 100, 3º 50€', NULL);

INSERT INTO `ESTABLECIMIENTO` (`CIF`, `PASSWORD`, `NOMBRE_CONCURSO`, `NOMBRE`, `LOCALIZACION`, `EMAIL`) 
VALUES ('A12345678', '123456', 'Certamen de Pinchos OU15', 'Solinas', 'O Couto', 'solinas@gmail.com'), 
       ('B12345678', '123456', 'Certamen de Pinchos OU15', 'Miudiño', 'Zona Vinos', 'miudiño@gmail.com'),
       ('C12345678', '123456', 'Certamen de Pinchos OU15', 'Meigallo', 'Centro', 'meigallo@gmail.com'),
       ('D12345678', '123456', 'Certamen de Pinchos OU15', 'Latino', 'Zona Vinos', 'latino@gmail.com'),
       ('E12345678', '123456', 'Certamen de Pinchos OU15', 'LaBull', 'Zona Vinos', 'labull@gmail.com');

INSERT INTO `PINCHO` (`NOMBRE`, `EMAIL_EST`, `DESCRIPCION`, `PRECIO`, `INGREDIENTES`, `FOTO`, `CELIACOS`, `VALIDACION`) 
VALUES ('Pincho Moruno', 'miudiño@gmail.com', 'Descripción', '5', 'Ingredientes', NULL, '1', '1'), 
	   ('Pincho burguer', 'solinas@gmail.com', 'Descripción', '3', 'Ingredientes', NULL, '0', '1'),
	   ('Bacalao frito', 'meigallo@gmail.com', 'Descripción', '6', 'Ingredientes', NULL, '1', '1'),
	   ('Huevo roto', 'latino@gmail.com', 'Descripción', '9', 'Ingredientes', NULL, '0', '1'),
	   ('Loco tomate', 'labull@gmail.com', 'Descripción', '13', 'Ingredientes', NULL, '1', '0'),
	   ('Pechuga grande', 'labull@gmail.com', 'Descripción', '2', 'Ingredientes', NULL, '1', '0'),
	   ('Mejillon morado', 'labull@gmail.com', 'Descripción', '5', 'Ingredientes', NULL, '1', '0');

INSERT INTO `USUARIO` (`USERNAME`, `NOMBRE`, `APELLIDOS`, `PASSWORD`, `TIPO`, `EMAIL`, `PROFESION`) 
VALUES ('isgonzalez', 'Isaac', 'Gonzalez', '123456', 'ORGANIZADOR', 'igdx1989@gmail.com', NULL), 
	   ('daroman', 'David', 'Roman', '123456', 'JURADO PROFESIONAL', 'davidroman2494@hotmail.com', 'Camarero'),
	   ('darodriguez', 'Daniel', 'Rodriguez', '123456', 'JURADO PROFESIONAL', 'daro@hotmail.com', 'Cocinero'),
	   ('rocortes', 'Romina', 'Cortes', '123456', 'JURADO PROFESIONAL', 'rocor@hotmail.com', 'Chef'),
       ('toferreiro', 'Toni', 'Ferreiro', '123456', 'JURADO POPULAR', 'ferreirocouto@hotmail.com', NULL),
	   ('matallon', 'Manuel', 'Tallon', '123456', 'JURADO POPULAR', 'mltallon@hotmail.com', NULL),
       ('luperez', 'Lucia', 'Perez', '123456', 'JURADO POPULAR', 'luciap@hotmail.com', NULL),
	   ('malopez', 'Marta', 'Lopez', '123456', 'JURADO POPULAR', 'martalo@hotmail.com', NULL),
       ('pecortana', 'Pedro', 'Cortana', '123456', 'JURADO POPULAR', 'pedrocor@hotmail.com', NULL),
	   ('maricon', 'Manuel', 'Ricon', '123456', 'JURADO POPULAR', 'mricon@hotmail.com', NULL);

INSERT INTO `CODIGO` (`CODIGO`, `NOMBRE_PINCHO`, `EMAIL_EST`, `EMAIL_USER`) 
VALUES ('MIUDIÑO@GMAIL.COM8888888888', 'Pincho Moruno', 'miudiño@gmail.com', NULL), 
	   ('SOLINAS@GMAIL.COM7777777777', 'Pincho burguer', 'solinas@gmail.com', NULL), 
	   ('SOLINAS@GMAIL.COM6666666666', 'Pincho burguer', 'solinas@gmail.com', 'ferreirocouto@hotmail.com'), 
	   ('MIUDIÑO@GMAIL.COM999999999', 'Pincho Moruno', 'miudiño@gmail.com', 'ferreirocouto@hotmail.com'), 
	   ('MEIGALLO@GMAIL.COM22222222', 'Bacalao frito', 'meigallo@gmail.com', 'mltallon@hotmail.com'), 
	   ('MEIGALLO@GMAIL.COM999999999', 'Bacalao frito', 'meigallo@gmail.com', 'ferreirocouto@hotmail.com');

INSERT INTO `ASIGNA_PINCHO` (`EMAIL_USER`, `NOMBRE_PINCHO`, `EMAIL_EST`) 
VALUES ('davidroman2494@hotmail.com', 'Pincho burguer', 'solinas@gmail.com'), 
       ('davidroman2494@hotmail.com', 'Bacalao frito', 'meigallo@gmail.com');

INSERT INTO `OPINA_POPULAR` (`ID_COMMENT`, `EMAIL_USER`, `NOMBRE_PINCHO`, `EMAIL_EST`, `COMENTARIO_POPULAR`) 
VALUES ('1', 'mltallon@hotmail.com', 'Bacalao frito', 'meigallo@gmail.com', 'El pincho no está mal, mejor la compañía ;)'), 
       ('2', 'ferreirocouto@hotmail.com', 'Bacalao frito', 'meigallo@gmail.com', 'Me da asquito el pescado'), 
       ('3', 'ferreirocouto@hotmail.com', 'Pincho Burguer', 'solinas@gmail.com', 'De lo mejorcito de este concurso'), 
       ('4', 'ferreirocouto@hotmail.com', 'Pincho Moruno', 'miudiño@gmail.com', 'Me encanta, aunque despues me repitió algo');